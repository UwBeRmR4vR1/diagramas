﻿using System;
using System.Text.RegularExpressions;
namespace NClass.Core
{
	internal class JavaParameterCollection : ParameterCollection
	{
		// <type> <name> [,]
		const string JavaParameterPattern =
			@"\s*(?<type>" + SyntaxHelper.GenericTypePattern2 + @")\s+" +
			@"(?<name>" + SyntaxHelper.NamePattern + @")\s*(,|$)";
		const string ParameterStringPattern = @"^\s*(" + JavaParameterPattern + ")*$";

		static Regex parameterRegex =
			new Regex(JavaParameterPattern, RegexOptions.ExplicitCapture);
		static Regex singleParamterRegex =
			new Regex("^" + JavaParameterPattern + "$", RegexOptions.ExplicitCapture);
		static Regex parameterStringRegex =
			new Regex(ParameterStringPattern, RegexOptions.ExplicitCapture);

		internal JavaParameterCollection()
		{
		}

		/// <exception cref="ArgumentNullException">
		/// <paramref name="collection"/> is null.
		/// </exception>
		internal JavaParameterCollection(JavaParameterCollection collection)
			: base(collection)
		{
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="declaration"/> does not fit to the syntax.
		/// </exception>
		/// <exception cref="ReservedNameException">
		/// The parameter name is already exists.
		/// </exception>
		public override Parameter Add(string declaration)
		{
			Match match = singleParamterRegex.Match(declaration);

			if (match.Success) {
				Group nameGroup = match.Groups["name"];
				Group typeGroup = match.Groups["type"];

				if (ReservedName(nameGroup.Value))
					throw new ReservedNameException(nameGroup.Value);

				Parameter parameter = new JavaParameter(nameGroup.Value, typeGroup.Value);
				InnerList.Add(parameter);

				return parameter;			
			}
			else {
				throw new BadSyntaxException(
					"error_invalid_parameter_declaration");
			}
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="declaration"/> does not fit to the syntax.
		/// </exception>
		/// <exception cref="ReservedNameException">
		/// The parameter name is already exists.
		/// </exception>
		public override Parameter ModifyParameter(Parameter parameter, string declaration)
		{
			Match match = singleParamterRegex.Match(declaration);
			int index = InnerList.IndexOf(parameter);

			if (index < 0)
				return parameter;

			if (match.Success) {
				Group nameGroup = match.Groups["name"];
				Group typeGroup = match.Groups["type"];

				if (ReservedName(nameGroup.Value, index))
					throw new ReservedNameException(nameGroup.Value);

				Parameter newParameter = new JavaParameter(nameGroup.Value, typeGroup.Value);
				InnerList[index] = newParameter;
				return newParameter;
			}
			else {
				throw new BadSyntaxException(
					"error_invalid_parameter_declaration");
			}
		}

		public override object Clone()
		{
			return new JavaParameterCollection(this);
		}
		
		/// <exception cref="BadSyntaxException">
		/// The <paramref name="declaration"/> does not fit to the syntax.
		/// </exception>
		public override void InitFromString(string declaration)
		{
			if (parameterStringRegex.IsMatch(declaration)) {
				Clear();
				foreach (Match match in parameterRegex.Matches(declaration)) {
					Group nameGroup = match.Groups["name"];
					Group typeGroup = match.Groups["type"];

					InnerList.Add(new JavaParameter(nameGroup.Value, typeGroup.Value));
				}
			}
			else {
				throw new BadSyntaxException(
					"error_invalid_parameter_declaration");
			}
		}
	}
}
