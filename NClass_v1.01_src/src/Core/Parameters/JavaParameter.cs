namespace NClass.Core
{
	internal sealed class JavaParameter : Parameter
	{
		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> or <paramref name="type"/> 
		/// does not fit to the syntax.
		/// </exception>
		internal JavaParameter(string name, string type)
			: base(name, type, ParameterModifier.None)
		{
		}

		public override Language Language
		{
			get { return Language.Java; }
		}
	}
}
