
namespace NClass.Core
{
	public enum ParameterModifier
	{
		None,
		Ref,
		Out,
		Params
	}
}
