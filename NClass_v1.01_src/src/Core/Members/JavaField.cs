using System;
using System.Text;
using System.Text.RegularExpressions;

namespace NClass.Core
{
	internal sealed class JavaField : Field
	{
		const string FinalPattern = @"(?<final>final\s+)?";
		const string FieldModifiersPattern = StaticPattern + FinalPattern;

		// [<access>] [static] [final] <type> <name> [= <initvalue>]
		const string FieldPattern =
			@"^\s*" + JavaAccessPattern + FieldModifiersPattern +
			@"(?<type>" + SyntaxHelper.GenericTypePattern2 + @")\s+" +
			@"(?<name>" + SyntaxHelper.NamePattern + @")" +
			@"(\s*=\s*(?<initvalue>[^\s](.*[^\s])?))?" + SyntaxHelper.DeclarationEnding;

		static Regex fieldRegex = new Regex(FieldPattern, RegexOptions.ExplicitCapture);

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> does not fit to the syntax.
		/// </exception>
		/// <exception cref="ArgumentNullException">
		/// <paramref name="parent"/> is null.
		/// </exception>
		internal JavaField(String name, OperationContainer parent) : base(name, parent)
		{
		}

		public override Language Language
		{
			get { return Language.Java; }
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="declaration"/> does not fit to the syntax.
		/// </exception>
		public override void InitFromString(string declaration)
		{
			Match match = fieldRegex.Match(declaration);

			if (match.Success) {
				try {
					Group nameGroup     = match.Groups["name"];
					Group typeGroup     = match.Groups["type"];
					Group accessGroup   = match.Groups["access"];
					Group staticGroup   = match.Groups["static"];
					Group readonlyGroup = match.Groups["final"];
					Group initGroup     = match.Groups["initvalue"];

					Name = nameGroup.Value;
					Type = typeGroup.Value;
					AccessModifier = SyntaxHelper.ParseAccessModifier(accessGroup.Value);
					IsStatic = staticGroup.Success;
					IsReadonly = readonlyGroup.Success;
					InitialValue = (initGroup.Success) ? initGroup.Value : "";
				}
				catch (BadSyntaxException ex) {
					throw new BadSyntaxException("error_invalid_declaration");
				}
			}
			else {
				throw new BadSyntaxException("error_invalid_declaration");
			}
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder(50);

			if (AccessModifier != AccessModifier.Default) {
				builder.Append(SyntaxHelper.GetAccessModifier(AccessModifier, Language.CSharp));
				builder.Append(" ");
			}

			if (IsStatic)
				builder.Append("static ");
			if (IsReadonly)
				builder.Append("final ");

			builder.AppendFormat("{0} {1}", Type, Name);
			if (HasInitialValue)
				builder.AppendFormat(" = {0}", InitialValue);

			return builder.ToString();
		}
	}
}
