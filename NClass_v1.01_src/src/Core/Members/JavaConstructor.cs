using System;
using System.Text.RegularExpressions;

namespace NClass.Core
{
	internal sealed class JavaConstructor : Constructor
	{
		// [<access>] <name>([<args>])
		const string ConstructorPattern =
			@"^\s*" + JavaAccessPattern +
			@"(?<name>" + SyntaxHelper.NamePattern + ")" +
			@"\((?(static)|(?<args>.*))\)" + SyntaxHelper.DeclarationEnding;

		static Regex constructorRegex =
			new Regex(ConstructorPattern, RegexOptions.ExplicitCapture);

		/// <exception cref="ArgumentNullException">
		/// <paramref name="parent"/> is null.
		/// </exception>
		internal JavaConstructor(OperationContainer parent) : base(parent)
		{
		}		

		public override AccessModifier DefaultAccess
		{
			get
			{
				return Parent.Access;
			}
		}

		/// <exception cref="BadSyntaxException">
		/// Cannot set static modifier.
		/// </exception>
		public override bool IsStatic
		{
			get
			{
				return base.IsStatic;
			}
			set
			{
				if (value)
					throw new BadSyntaxException("error_cannot_set_static");
			}
		}

		public override Language Language
		{
			get { return Language.Java; }
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="declaration"/> does not fit to the syntax.
		/// </exception>
		public override void InitFromString(string declaration)
		{
			Match match = constructorRegex.Match(declaration);

			if (match.Success) {
				try {
					Group nameGroup   = match.Groups["name"];
					Group accessGroup = match.Groups["access"];
					Group argsGroup   = match.Groups["args"];

					Name = nameGroup.Value;
					AccessModifier = SyntaxHelper.ParseAccessModifier(accessGroup.Value);
					ParameterList.InitFromString(argsGroup.Value);
				}
				catch (BadSyntaxException ex) {
					throw new BadSyntaxException("error_invalid_declaration");
				}
			}
			else {
				throw new BadSyntaxException("error_invalid_declaration");
			}
		}
	}
}
