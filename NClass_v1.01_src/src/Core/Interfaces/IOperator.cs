namespace NClass.Core
{
	public interface IOperator
	{
		bool IsOperator
		{
			get;
		}

		bool IsConversionOperator
		{
			get;
		}
	}
}
