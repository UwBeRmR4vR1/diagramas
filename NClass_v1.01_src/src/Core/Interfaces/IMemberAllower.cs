using System;

namespace NClass.Core
{
	public interface IMemberAllower
	{
		void RemoveMember(Member member);
	}
}
