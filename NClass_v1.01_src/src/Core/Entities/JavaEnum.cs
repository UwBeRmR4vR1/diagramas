using System;

namespace NClass.Core
{
	internal sealed class JavaEnum : EnumType
	{
		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> does not fit to the syntax.
		/// </exception>
		internal JavaEnum(string name) : base(name)
		{
		}

		public override string Signature
		{
			get
			{
				return SyntaxHelper.GetAccessModifier(
					Access, Language.Java, false) + " Enum";
			}
		}

		public override Language Language
		{
			get { return Language.Java; }
		}
	}
}
