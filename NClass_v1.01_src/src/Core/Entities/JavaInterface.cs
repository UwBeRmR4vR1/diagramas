using System;
using System.Collections.Generic;

namespace NClass.Core
{
	internal sealed class JavaInterface : InterfaceType, IFieldAllower
	{
		List<JavaField> fieldList;

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> does not fit to the syntax.
		/// </exception>
		public JavaInterface(string name) : base(name)
		{
			fieldList = new List<JavaField>();
		}

		public IEnumerable<Field> Fields
		{
			get
			{
				for (int i = 0; i < FieldList.Count; i++)
					yield return FieldList[i];
			}
		}

		private List<JavaField> FieldList
		{
			get { return fieldList; }
		}

		public int FieldCount
		{
			get { return FieldList.Count; }
		}

		public override AccessModifier DefaultMemberAccess
		{
			get { return Access; }
		}

		public override string Signature
		{
			get
			{
				return SyntaxHelper.GetAccessModifier(
					Access, Language.Java, false) + " Interface";
			}
		}

		public override Language Language
		{
			get { return Language.Java; }
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> does not fit to the syntax.
		/// </exception>
		public Field AddField(string name)
		{
			JavaField field = new JavaField(name, this);

			field.AccessModifier = AccessModifier.Default;
			field.IsStatic = true;
			field.IsReadonly = true;
			FieldList.Add(field);
			return field;
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> does not fit to the syntax.
		/// </exception>
		public override Method AddMethod(string name)
		{
			Method method = new JavaMethod(name, this);

			method.Modifier = OperationModifier.None;
			method.AccessModifier = AccessModifier.Default;
			OperationList.Add(method);
			return method;
		}
	}
}
