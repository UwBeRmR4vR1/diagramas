﻿namespace NClass.Translations
{
	public enum DisplayLanguage
	{
		Default,
		English,
		Hungarian
	}
}
