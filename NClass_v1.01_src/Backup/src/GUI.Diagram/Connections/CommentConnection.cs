// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using NClass.Core;

namespace NClass.GUI.Diagram
{
	internal sealed class CommentConnection : Connection
	{
		CommentRelation commentRelation;

		/// <exception cref="ArgumentNullException">
		/// <paramref name="startNode"/>is null.-or-
		/// <paramref name="endNode"/> is null.-or-
		/// <paramref name="commentRelation"/> is null.
		/// </exception>
		internal CommentConnection(TerminalNode startNode, TerminalNode endNode,
			CommentRelation commentRelation) : base(startNode, endNode)
		{
			if (commentRelation == null)
				throw new ArgumentNullException("commentRelation");

			this.commentRelation = commentRelation;
		}

		public override Relation Relation
		{
			get { return commentRelation; }
		}

		public override string ToString()
		{
			return commentRelation.ToString();
		}
	}
}
