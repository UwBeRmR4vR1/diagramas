// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Drawing;
using NClass.Core;

namespace NClass.GUI.Diagram
{
	internal sealed class GeneralizationConnection : Connection
	{
		const int TriangleWidth = 12;
		const int TriangleHeight = 17;

		static Point[] trianglePoints = {
			new Point(-TriangleWidth / 2, TriangleHeight),
			new Point(0, 0),
			new Point(TriangleWidth / 2, TriangleHeight)
		};

		Generalization generalization;

		/// <exception cref="ArgumentNullException">
		/// <paramref name="startNode"/>is null.-or-
		/// <paramref name="endNode"/> is null.-or-
		/// <paramref name="generalization"/> is null.
		/// </exception>
		internal GeneralizationConnection(TerminalNode startNode, TerminalNode endNode,
			Generalization generalization) : base(startNode, endNode)
		{
			if (generalization == null)
				throw new ArgumentNullException("generalization");

			this.generalization = generalization;
		}

		public override Relation Relation
		{
			get { return generalization; }
		}

		protected override void DrawRelativeEndSign(Graphics g)
		{
			base.DrawRelativeEndSign(g);

			g.FillPolygon(LightBrush, trianglePoints);
			g.DrawPolygon(SolidPen, trianglePoints);
		}

		public override string ToString()
		{
			return generalization.ToString();
		}
	}
}
