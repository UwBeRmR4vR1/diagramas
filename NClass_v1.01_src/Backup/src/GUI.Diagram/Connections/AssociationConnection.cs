// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Drawing;
using NClass.Core;

namespace NClass.GUI.Diagram
{
	internal sealed class AssociationConnection : Connection
	{
		const int ArrowWidth = 12;
		const int ArrowHeight = 17;
		const int DiamondWidth = 10;
		const int DiamondHeight = 18;

		static Point[] diamondPoints =  {
			new Point(0, 0),
			new Point(DiamondWidth / 2, DiamondHeight / 2),
			new Point(0, DiamondHeight),
			new Point(-DiamondWidth / 2, DiamondHeight / 2)
		};

		Association association;

		/// <exception cref="ArgumentNullException">
		/// <paramref name="startNode"/>is null.-or-
		/// <paramref name="endNode"/> is null.-or-
		/// <paramref name="association"/> is null.
		/// </exception>
		internal AssociationConnection(TerminalNode startNode, TerminalNode endNode,
			Association association) : base(startNode, endNode)
		{
			if (association == null)
				throw new ArgumentNullException("association");

			this.association = association;
		}

		public override Relation Relation
		{
			get { return association; }
		}

		protected override void DrawRelativeStartSign(Graphics g)
		{
			base.DrawRelativeStartSign(g);

			if (association.IsAggregation) {
				g.FillPolygon(LightBrush, diamondPoints);
				g.DrawPolygon(SolidPen, diamondPoints);
			}
			else if (association.IsComposition) {
				g.FillPolygon(DarkBrush, diamondPoints);
			}
			else if (association.Direction == Direction.DestinationSource) {
				g.DrawLine(SolidPen,  ArrowWidth / 2, ArrowHeight, 0, 0);
				g.DrawLine(SolidPen, -ArrowWidth / 2, ArrowHeight, 0, 0);
			}
		}

		protected override void DrawRelativeEndSign(Graphics g)
		{
			base.DrawRelativeEndSign(g);

			if (association.Direction == Direction.SourceDestination) {
				g.DrawLine(SolidPen,  ArrowWidth / 2, ArrowHeight, 0, 0);
				g.DrawLine(SolidPen, -ArrowWidth / 2, ArrowHeight, 0, 0);
			}
		}

		public override string ToString()
		{
			return association.ToString();
		}
	}
}
