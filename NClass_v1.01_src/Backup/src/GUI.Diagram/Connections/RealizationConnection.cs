// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using NClass.Core;

namespace NClass.GUI.Diagram
{
	internal sealed class RealizationConnection : Connection
	{
		const int TriangleWidth = 12;
		const int TriangleHeight = 17;

		static Point[] trianglePoints = {
			new Point(-TriangleWidth / 2, TriangleHeight),
			new Point(0, 0),
			new Point(TriangleWidth / 2, TriangleHeight)
		};

		Realization realization;

		/// <exception cref="ArgumentNullException">
		/// <paramref name="startNode"/>is null.-or-
		/// <paramref name="endNode"/> is null.-or-
		/// <paramref name="realization"/> is null.
		/// </exception>
		internal RealizationConnection(TerminalNode startNode, TerminalNode endNode,
			Realization realization) : base(startNode, endNode)
		{
			if (realization == null)
				throw new ArgumentNullException("realization");

			this.realization = realization;
		}

		public override Relation Relation
		{
			get { return realization; }
		}

		protected override bool Dashed
		{
			get { return true; }
		}

		protected override void DrawRelativeEndSign(Graphics g)
		{
			base.DrawRelativeEndSign(g);

			g.FillPolygon(LightBrush, trianglePoints);
			g.DrawPolygon(SolidPen, trianglePoints);
		}

		public override string ToString()
		{
			return realization.ToString();
		}
	}
}
