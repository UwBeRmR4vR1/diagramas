// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using NClass.Core;

namespace NClass.GUI.Diagram
{
	internal sealed class DependencyConnection : Connection
	{
		const int ArrowWidth = 12;
		const int ArrowHeight = 17;

		Dependency dependency;

		/// <exception cref="ArgumentNullException">
		/// <paramref name="startNode"/>is null.-or-
		/// <paramref name="endNode"/> is null.-or-
		/// <paramref name="dependency"/> is null.
		/// </exception>
		internal DependencyConnection(TerminalNode startNode, TerminalNode endNode,
			Dependency dependency) : base(startNode, endNode)
		{
			if (dependency == null)
				throw new ArgumentNullException("dependency");

			this.dependency = dependency;
		}

		public override Relation Relation
		{
			get { return dependency; }
		}

		protected override bool Dashed
		{
			get
			{
				return true;
			}
		}

		protected override void DrawRelativeEndSign(Graphics g)
		{
			base.DrawRelativeEndSign(g);

			g.DrawLine(SolidPen,  ArrowWidth / 2, ArrowHeight, 0, 0);
			g.DrawLine(SolidPen, -ArrowWidth / 2, ArrowHeight, 0, 0);
		}

		public override string ToString()
		{
			return dependency.ToString();
		}
	}
}
