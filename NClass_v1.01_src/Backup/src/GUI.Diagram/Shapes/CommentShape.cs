// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using NClass.Core;
using NClass.Translations;

namespace NClass.GUI.Diagram
{
	public sealed class CommentShape : EntityShape
	{
		const int TextTransparency = 128;

		static Pen borderPen = new Pen(Color.Black);
		static SolidBrush backgroundBrush = new SolidBrush(Color.LightYellow);
		static SolidBrush textBrush = new SolidBrush(Color.Black);
		static SolidBrush transparentTextBrush = new SolidBrush(
			Color.FromArgb(TextTransparency, Color.Black));

		Comment comment;
		ToolStripMenuItem mnuEdit;

		/// <exception cref="ArgumentNullException">
		/// <paramref name="comment"/> is null.
		/// </exception>
		internal CommentShape(Comment comment)
		{
			if (comment == null)
				throw new ArgumentNullException("comment");

			this.comment = comment;
			Text = comment.Text;
		}

		public override Entity Entity
		{
			get { return comment; }
		}

		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				if (value != Text)
					OnContentsChanged(EventArgs.Empty);
				base.Text = value;
			}
		}

		protected override Size DefaultSize
		{
			get
			{
				return new Size(162, 72);
			}
		}

		private int MarginSize
		{
			get { return Style.CurrentStyle.CommentMarginSize; }
		}

		private RectangleF MarginBounds
		{
			get
			{
				return new RectangleF(MarginSize, MarginSize,
					Width - 2 * MarginSize, Height - 2 * MarginSize);
			}
		}

		private void mnuEditComment_Click(object sender, EventArgs e)
		{
			EditText();
		}

		protected override void OnDoubleClick(EventArgs e)
		{
			EditText();
		}

		private void EditText()
		{
			using (EditCommentDialog dialog = new EditCommentDialog(Text)) {
				if (dialog.ShowDialog() == DialogResult.OK) {
					Text = dialog.InputText;
					comment.Text = dialog.InputText;
					Invalidate();
				}
			}
		}

		protected override void InitContextMenu()
		{
			base.InitContextMenu();

			mnuEdit = new ToolStripMenuItem(Texts.GetText("menu_edit_comment"),
				Properties.Resources.EditComment, new EventHandler(mnuEditComment_Click));

			ContextMenuStrip.Items.Add("-");
			ContextMenuStrip.Items.Add(mnuEdit);
		}

		protected override void UpdateContextMenu()
		{
			base.UpdateContextMenu();
			mnuEdit.Text = Texts.GetText("menu_edit_comment");
		}

		private void UpdateStyles(bool onScreen)
		{
			textBrush.Color = Style.CurrentStyle.CommentTextColor;
			transparentTextBrush.Color = Color.FromArgb(TextTransparency, textBrush.Color);
			backgroundBrush.Color = Style.CurrentStyle.CommentBackColor;

			if (IsSelected && onScreen) {
				borderPen.Color = Style.CurrentStyle.CommentSelectedBorderPenColor;
				borderPen.Width = Style.CurrentStyle.CommentSelectedBorderWidth;
				borderPen.DashStyle = (Style.CurrentStyle.IsCommentSelectedBorderDashed) ?
					DashStyle.Dash : DashStyle.Solid;
			}
			else {
				borderPen.Color = Style.CurrentStyle.CommentBorderColor;
				borderPen.Width = Style.CurrentStyle.CommentBorderWidth;
				borderPen.DashStyle = (Style.CurrentStyle.IsCommentBorderDashed) ?
					DashStyle.Dash : DashStyle.Solid;
			}
			borderPen.DashCap = (borderPen.Width == 1) ? DashCap.Round : DashCap.Flat;
		}

		private void DrawSurface(Graphics g)
		{
			GraphicsPath path = new GraphicsPath();
			int padding = (int) borderPen.Width / 2;

			// Drawing borders & background
			path.AddLine(padding, padding, Width - MarginSize - padding, padding);
			path.AddLine(Width - padding - 1, MarginSize + padding - 1,
				Width - padding- 1, Height - padding - 1);
			path.AddLine(Width - padding - 1, Height - padding - 1,
				padding, Height - padding - 1);
			path.CloseFigure();

			g.FillPath(backgroundBrush, path);
			g.DrawPath(borderPen, path);

			// Drawing earmark
			if (Style.CurrentStyle.CommentMarginSize > 0) {
				path.Reset();
				path.AddLine(Width - MarginSize - padding, padding,
					Width - MarginSize - padding, MarginSize + padding - 1);
				path.AddLine(Width - MarginSize - padding, MarginSize + padding - 1,
					Width - padding - 1, MarginSize + padding - 1);
				g.DrawPath(borderPen, path);
			}

			path.Dispose();
		}

		private void DrawText(Graphics g, bool onScreen)
		{
			if (Text == "" && onScreen) {
				g.DrawString(Texts.GetText("double_click_to_edit"),
					Style.CurrentStyle.CommentFont, transparentTextBrush, MarginBounds);
			}
			else {
				g.DrawString(Text, Style.CurrentStyle.CommentFont, textBrush, MarginBounds);
			}
		}

		protected override void Draw(Graphics g, Point position, float zoom, bool onScreen)
		{
			if (zoom == 1)
				g.SmoothingMode = SmoothingMode.AntiAlias;
			g.ScaleTransform(zoom, zoom);
			g.TranslateTransform(position.X, position.Y);

			UpdateStyles(onScreen);
			DrawSurface(g);
			DrawText(g, onScreen);
			g.ResetTransform();
		}

		public override string ToString()
		{
			return Texts.GetText("comment");
		}
	}
}
