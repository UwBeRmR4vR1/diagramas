﻿// NClass - UML class diagram editor
// Copyright (C) 2006 Balázs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using NClass.Core;

namespace NClass.GUI.Diagram
{
	public sealed class InterfaceShape : OperationContainerShape
	{
		InterfaceType _interface;

		/// <exception cref="ArgumentNullException">
		/// <paramref name="interfaceType"/> is null.
		/// </exception>
		internal InterfaceShape(InterfaceType interfaceType)
		{
			if (interfaceType == null)
				throw new ArgumentNullException("interfaceType");

			_interface = interfaceType;
		}

		public override Entity Entity
		{
			get { return _interface; }
		}

		protected override Color BackgroundColor
		{
			get { return Style.CurrentStyle.InterfaceBackgroundColor; }
		}

		protected override Color BorderColor
		{
			get { return Style.CurrentStyle.InterfaceBorderColor; }
		}

		protected override int BorderWidth
		{
			get { return Style.CurrentStyle.InterfaceBorderWidth; }
		}

		protected override bool IsBorderDashed
		{
			get { return Style.CurrentStyle.IsInterfaceBorderDashed; }
		}

		protected override bool IsSelectedBorderDashed
		{
			get { return Style.CurrentStyle.IsInterfaceSelectedBorderDashed; }
		}

		protected override Color HeaderColor
		{
			get { return Style.CurrentStyle.InterfaceHeaderColor; }
		}

		protected override int RoundingSize
		{
			get { return Style.CurrentStyle.InterfaceRoundingSize; }
		}

		protected override Color SelectedBorderColor
		{
			get { return Style.CurrentStyle.InterfaceSelectedBorderColor; }
		}

		protected override int SelectedBorderWidth
		{
			get { return Style.CurrentStyle.InterfaceSelectedBorderWidth; }
		}

		protected override bool UseGradientHeader
		{
			get { return Style.CurrentStyle.InterfaceUseGradientHeader; }
		}
	}
}
