﻿// NClass - UML class diagram editor
// Copyright (C) 2006 Balázs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using NClass.Core;

namespace NClass.GUI.Diagram
{
	public sealed class EnumShape : TypeShape
	{
		static EnumDialog enumDialog = new EnumDialog();
		static SolidBrush itemBrush = new SolidBrush(Color.Black);

		EnumType _enum;

		/// <exception cref="ArgumentNullException">
		/// <paramref name="enumType"/> is null.
		/// </exception>
		internal EnumShape(EnumType enumType)
		{
			if (enumType == null)
				throw new ArgumentNullException("enumType");

			_enum = enumType;
		}

		public override Entity Entity
		{
			get { return _enum; }
		}

		protected override Color BackgroundColor
		{
			get { return Style.CurrentStyle.EnumBackgroundColor; }
		}

		protected override Color BorderColor
		{
			get { return Style.CurrentStyle.EnumBorderColor; }
		}

		protected override int BorderWidth
		{
			get { return Style.CurrentStyle.EnumBorderWidth; }
		}

		protected override bool IsBorderDashed
		{
			get { return Style.CurrentStyle.IsEnumBorderDashed; }
		}

		protected override bool IsSelectedBorderDashed
		{
			get { return Style.CurrentStyle.IsEnumSelectedBorderDashed; }
		}

		protected override Color HeaderColor
		{
			get { return Style.CurrentStyle.EnumHeaderColor; }
		}

		protected override int RoundingSize
		{
			get { return Style.CurrentStyle.EnumRoundingSize; }
		}

		protected override Color SelectedBorderColor
		{
			get { return Style.CurrentStyle.EnumSelectedBorderColor; }
		}

		protected override int SelectedBorderWidth
		{
			get { return Style.CurrentStyle.EnumSelectedBorderWidth; }
		}

		protected override bool UseGradientHeader
		{
			get { return Style.CurrentStyle.EnumUseGradientHeader; }
		}

		protected override void EditItems()
		{
			if (Entity is EnumType) {
				enumDialog.ShowDialog((EnumType) Entity);
				if (enumDialog.Changed)
					OnContentsChanged(EventArgs.Empty);
				Invalidate();
			}
		}

		private void DrawItem(Graphics g, float height, EnumItem item)
		{
			if (item == null)
				return;

			int marginSize = Style.CurrentStyle.MarginSize;
			itemBrush.Color = Style.CurrentStyle.EnumItemColor;

			if (Style.CurrentStyle.UseIcons) {
				float fontHeight = Font.GetHeight(g);
				float iconHeight = Properties.Resources.EnumItem.Height;
				float recordHeight = Math.Max(fontHeight, iconHeight);

				if (recordHeight > iconHeight) {
					g.DrawImage(Properties.Resources.EnumItem,
						new PointF(marginSize, height + (int) ((recordHeight - iconHeight) / 2)));
				}
				else {
					g.DrawImage(Properties.Resources.EnumItem, new PointF(marginSize, height));
				}

				g.DrawString(item.ToString(), Font, itemBrush,
					new PointF(marginSize + Properties.Resources.EnumItem.Width + IconSpacing,
					height + (float) Math.Ceiling((recordHeight - fontHeight) / 2)));
			}
			else {
				g.DrawString(item.ToString(), Font, itemBrush,
					new PointF(marginSize, height));
			}
		}

		protected override void DrawContent(Graphics g)
		{
			float height = HeaderHeight + Style.CurrentStyle.MarginSize;
			float fontHeight = Font.GetHeight(g) + 1;

			if (Style.CurrentStyle.UseIcons)
				fontHeight = Math.Max(fontHeight, Icons.IconSize.Height + 1);

			if (Entity is EnumType) {
				foreach (EnumItem item in ((EnumType) Entity).Values) {
					DrawItem(g, height, item);
					height += fontHeight;
				}
			}

			MinHeight = (int) height + Style.CurrentStyle.MarginSize;
		}
	}
}
