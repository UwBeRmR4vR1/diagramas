﻿// NClass - UML class diagram editor
// Copyright (C) 2006 Balázs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using NClass.Core;

namespace NClass.GUI.Diagram
{
	public sealed class DelegateShape : TypeShape
	{
		static DelegateDialog delegateDialog = new DelegateDialog();
		static SolidBrush parameterBrush = new SolidBrush(Color.Black);

		DelegateType _delegate;

		/// <exception cref="ArgumentNullException">
		/// <paramref name="delegateType"/> is null.
		/// </exception>
		internal DelegateShape(DelegateType delegateType)
		{
			if (delegateType == null)
				throw new ArgumentNullException("type");

			_delegate = delegateType;
		}

		public override Entity Entity
		{
			get { return _delegate; }
		}

		protected override Color BackgroundColor
		{
			get { return Style.CurrentStyle.DelegateBackgroundColor; }
		}

		protected override Color BorderColor
		{
			get { return Style.CurrentStyle.DelegateBorderColor; }
		}

		protected override int BorderWidth
		{
			get { return Style.CurrentStyle.DelegateBorderWidth; }
		}

		protected override bool IsBorderDashed
		{
			get { return Style.CurrentStyle.IsDelegateBorderDashed; }
		}

		protected override bool IsSelectedBorderDashed
		{
			get { return Style.CurrentStyle.IsDelegateSelectedBorderDashed; }
		}

		protected override Color HeaderColor
		{
			get { return Style.CurrentStyle.DelegateHeaderColor; }
		}

		protected override int RoundingSize
		{
			get { return Style.CurrentStyle.DelegateRoundingSize; }
		}

		protected override Color SelectedBorderColor
		{
			get { return Style.CurrentStyle.DelegateSelectedBorderColor; }
		}

		protected override int SelectedBorderWidth
		{
			get { return Style.CurrentStyle.DelegateSelectedBorderWidth; }
		}

		protected override bool UseGradientHeader
		{
			get { return Style.CurrentStyle.DelegateUseGradientHeader; }
		}

		protected override void EditItems()
		{
			if (Entity is DelegateType) {
				delegateDialog.ShowDialog((DelegateType) Entity);
				if (delegateDialog.Changed)
					OnContentsChanged(EventArgs.Empty);
				Invalidate();
			}
		}

		private void DrawItem(Graphics g, float height, Parameter parameter)
		{
			if (parameter == null)
				return;

			int marginSize = Style.CurrentStyle.MarginSize;
			parameterBrush.Color = Style.CurrentStyle.DelegateParameterColor;

			if (Style.CurrentStyle.UseIcons) {
				float fontHeight = Font.GetHeight(g);
				float iconHeight = Properties.Resources.EnumItem.Height;
				float recordHeight = Math.Max(fontHeight, iconHeight);

				if (recordHeight > iconHeight) {
					g.DrawImage(Properties.Resources.Parameter,
						new PointF(marginSize, height + (int) ((recordHeight - iconHeight) / 2)));
				}
				else {
					g.DrawImage(Properties.Resources.Parameter, new PointF(marginSize, height));
				}

				g.DrawString(parameter.ToString(), Font, parameterBrush,
					new PointF(marginSize + Properties.Resources.EnumItem.Width + IconSpacing,
					height + (float) Math.Ceiling((recordHeight - fontHeight) / 2)));
			}
			else {
				g.DrawString(parameter.ToString(), Font, parameterBrush,
					new PointF(marginSize, height));
			}
		}

		protected override void DrawContent(Graphics g)
		{
			float height = HeaderHeight + Style.CurrentStyle.MarginSize;
			float fontHeight = Font.GetHeight(g) + 1;

			if (Style.CurrentStyle.UseIcons)
				fontHeight = Math.Max(fontHeight, Icons.IconSize.Height + 1);

			if (Entity is DelegateType) {
				foreach (Parameter parameter in ((DelegateType) Entity).Parameters) {
					DrawItem(g, height, parameter);
					height += fontHeight;
				}
			}

			MinHeight = (int) height + Style.CurrentStyle.MarginSize;
		}
	}
}
