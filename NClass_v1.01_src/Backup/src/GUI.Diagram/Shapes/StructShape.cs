﻿// NClass - UML class diagram editor
// Copyright (C) 2006 Balázs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using NClass.Core;

namespace NClass.GUI.Diagram
{
	public sealed class StructShape : OperationContainerShape
	{
		StructType _struct;

		/// <exception cref="ArgumentNullException">
		/// <paramref name="structType"/> is null.
		/// </exception>
		internal StructShape(StructType structType)
		{
			if (structType == null)
				throw new ArgumentNullException("structType");

			_struct = structType;
		}

		public override Entity Entity
		{
			get { return _struct; }
		}

		protected override Color BackgroundColor
		{
			get { return Style.CurrentStyle.StructBackgroundColor; }
		}

		protected override Color BorderColor
		{
			get { return Style.CurrentStyle.StructBorderColor; }
		}

		protected override int BorderWidth
		{
			get { return Style.CurrentStyle.StructBorderWidth; }
		}

		protected override bool IsBorderDashed
		{
			get { return Style.CurrentStyle.IsStructBorderDashed; }
		}

		protected override bool IsSelectedBorderDashed
		{
			get { return Style.CurrentStyle.IsStructSelectedBorderDashed; }
		}

		protected override Color HeaderColor
		{
			get { return Style.CurrentStyle.StructHeaderColor; }
		}

		protected override int RoundingSize
		{
			get { return Style.CurrentStyle.StructRoundingSize; }
		}

		protected override Color SelectedBorderColor
		{
			get { return Style.CurrentStyle.StructSelectedBorderColor; }
		}

		protected override int SelectedBorderWidth
		{
			get { return Style.CurrentStyle.StructSelectedBorderWidth; }
		}

		protected override bool UseGradientHeader
		{
			get { return Style.CurrentStyle.StructUseGradientHeader; }
		}
	}
}
