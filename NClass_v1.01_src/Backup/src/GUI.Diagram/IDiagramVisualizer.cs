﻿// NClass - UML class diagram editor
// Copyright (C) 2006 Balázs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Xml;
using System.Drawing;
using NClass.Core;

namespace NClass.GUI.Diagram
{
	public interface IDiagramVisualizer
	{
		event EventHandler ContentsChanged;
		event EntityRemovedEventHandler EntityRemoved;
		event RelationRemovedEventHandler RelationRemoved;
		event ConnectionCreatedEventHandler ConnectionCreated;

		void Clear();

		/// <exception cref="ArgumentException">
		/// There is no type of <paramref name="entity"/>.
		/// </exception>
		/// <exception cref="ArgumentNullException">
		/// <paramref name="entity"/> is null.
		/// </exception>
		void AddEntity(Entity entity);

		/// <exception cref="ArgumentException">
		/// There is no type of <paramref name="relation"/>.
		/// </exception>
		/// <exception cref="ArgumentNullException">
		/// <paramref name="relation"/> is null.
		/// </exception>
		void AddRelation(Relation relation, bool fromScreen);

		void LoadingFinished();

		/// <exception cref="ArgumentNullException">
		/// <paramref name="node"/> is null.
		/// </exception>
		void Serialize(XmlNode node);

		/// <exception cref="ArgumentNullException">
		/// <paramref name="node"/> is null.
		/// </exception>
		void Deserialize(XmlNode node);
	}
}
