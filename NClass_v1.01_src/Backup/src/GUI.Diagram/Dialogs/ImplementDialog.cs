﻿// NClass - UML class diagram editor
// Copyright (C) 2006 Balázs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using NClass.Core;
using NClass.Translations;

namespace NClass.GUI.Diagram
{
	public partial class ImplementDialog : Form
	{
		bool locked = false;
		static int interfaceImageIndex = Icons.InterfaceImageIndex;

		public ImplementDialog()
		{
			InitializeComponent();
			treMembers.ImageList = Icons.IconList;
		}

		public IEnumerable<Operation> SelectedOperations
		{
			get
			{
				for (int parent = 0; parent < treMembers.Nodes.Count; parent++) {
					TreeNode parentNode = treMembers.Nodes[parent];
					for (int child = 0; child < parentNode.Nodes.Count; child++) {
						if (parentNode.Nodes[child].Tag is Operation && 
							parentNode.Nodes[child].Checked)
							yield return (Operation) parentNode.Nodes[child].Tag;						
					}
				}
			}
		}

		private void AddMembers(InterfaceType _interface)
		{
			if (_interface == null || _interface.OperationCount == 0)
				return;

			TreeNode node = treMembers.Nodes.Add(_interface.Name);
			node.SelectedImageIndex = interfaceImageIndex;
			node.ImageIndex = interfaceImageIndex;

			foreach (Operation operation in _interface.Operations) {
				TreeNode child = node.Nodes.Add(operation.GetCaption());
				int index = Icons.GetImageIndex(operation);

				child.ImageIndex = index;
				child.SelectedImageIndex = index;
				child.Tag = operation;
			}			
		}

		public DialogResult ShowDialog(IInterfaceImplementer implementer)
		{
			if (implementer == null)
				return DialogResult.None;

			treMembers.Nodes.Clear();
			foreach (InterfaceType _interface in implementer.Interfaces)
				AddMembers(_interface);

			return ShowDialog();
		}

		private void UpdateTexts()
		{
			this.Text = Texts.GetText("implement_interfaces");
			btnOK.Text = Texts.GetText("button_ok");
			btnCancel.Text = Texts.GetText("button_cancel");
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			UpdateTexts();
		}

		private void treMembers_AfterCheck(object sender, TreeViewEventArgs e)
		{
			if (!locked) {
				locked = true;

				TreeNode node = e.Node;
				TreeNode parentNode = e.Node.Parent;

				if (parentNode != null) {
					if (!node.Checked) {
						parentNode.Checked = false;
					}
					else {
						bool allChecked = true;

						foreach (TreeNode neighbour in parentNode.Nodes) {
							if (!neighbour.Checked) {
								allChecked = false;
								break;
							}
						}
						parentNode.Checked = allChecked;
					}
				}

				foreach (TreeNode child in node.Nodes)
					child.Checked = node.Checked;

				locked = false;
			}
		}
	}
}