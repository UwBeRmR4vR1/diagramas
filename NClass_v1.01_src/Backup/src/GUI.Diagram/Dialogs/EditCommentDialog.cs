﻿// NClass - UML class diagram editor
// Copyright (C) 2006 Balázs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Windows.Forms;
using NClass.Translations;

namespace NClass.GUI.Diagram
{
	public partial class EditCommentDialog : Form
	{
		public EditCommentDialog(string text)
		{
			InitializeComponent();
			txtInput.Text = text;
		}

		public string InputText
		{
			get { return txtInput.Text; }
		}

		private void UpdateTexts()
		{
			this.Text = Texts.GetText("edit_comment");
			lblEdit.Text = Texts.GetText("type_text:");
			btnOK.Text = Texts.GetText("button_ok");
			btnCancel.Text = Texts.GetText("button_cancel");
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			UpdateTexts();
		}
	}
}