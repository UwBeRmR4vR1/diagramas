﻿namespace NClass.GUI.Diagram
{
	partial class MembersDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MembersDialog));
			this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this.txtSyntax = new System.Windows.Forms.TextBox();
			this.lblSyntax = new System.Windows.Forms.Label();
			this.lblName = new System.Windows.Forms.Label();
			this.txtName = new System.Windows.Forms.TextBox();
			this.lblType = new System.Windows.Forms.Label();
			this.lblAccess = new System.Windows.Forms.Label();
			this.cboAccess = new System.Windows.Forms.ComboBox();
			this.chkStatic = new System.Windows.Forms.CheckBox();
			this.chkReadonly = new System.Windows.Forms.CheckBox();
			this.chkConst = new System.Windows.Forms.CheckBox();
			this.txtInitialValue = new System.Windows.Forms.TextBox();
			this.lblInitValue = new System.Windows.Forms.Label();
			this.lstMembers = new System.Windows.Forms.ListView();
			this.icon = new System.Windows.Forms.ColumnHeader();
			this.name = new System.Windows.Forms.ColumnHeader();
			this.type = new System.Windows.Forms.ColumnHeader();
			this.access = new System.Windows.Forms.ColumnHeader();
			this.modifier = new System.Windows.Forms.ColumnHeader();
			this.grpOtherModifiers = new System.Windows.Forms.GroupBox();
			this.chkNew = new System.Windows.Forms.CheckBox();
			this.cboType = new System.Windows.Forms.ComboBox();
			this.cboModifier = new System.Windows.Forms.ComboBox();
			this.lblModifier = new System.Windows.Forms.Label();
			this.btnClose = new System.Windows.Forms.Button();
			this.toolStrip = new System.Windows.Forms.ToolStrip();
			this.toolNewField = new System.Windows.Forms.ToolStripButton();
			this.toolNewMethod = new System.Windows.Forms.ToolStripButton();
			this.toolNewConstructor = new System.Windows.Forms.ToolStripButton();
			this.toolNewDestructor = new System.Windows.Forms.ToolStripButton();
			this.toolNewProperty = new System.Windows.Forms.ToolStripButton();
			this.toolNewEvent = new System.Windows.Forms.ToolStripButton();
			this.toolDelete = new System.Windows.Forms.ToolStripButton();
			this.toolSepMoving = new System.Windows.Forms.ToolStripSeparator();
			this.toolMoveDown = new System.Windows.Forms.ToolStripButton();
			this.toolMoveUp = new System.Windows.Forms.ToolStripButton();
			this.toolSepSorting = new System.Windows.Forms.ToolStripSeparator();
			this.toolSortByName = new System.Windows.Forms.ToolStripButton();
			this.toolSortByAccess = new System.Windows.Forms.ToolStripButton();
			this.toolSortByKind = new System.Windows.Forms.ToolStripButton();
			this.toolSepAddNew = new System.Windows.Forms.ToolStripSeparator();
			this.toolOverrideList = new System.Windows.Forms.ToolStripButton();
			this.toolImplementList = new System.Windows.Forms.ToolStripButton();
			((System.ComponentModel.ISupportInitialize) (this.errorProvider)).BeginInit();
			this.grpOtherModifiers.SuspendLayout();
			this.toolStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// errorProvider
			// 
			this.errorProvider.ContainerControl = this;
			// 
			// txtSyntax
			// 
			this.txtSyntax.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.txtSyntax.Location = new System.Drawing.Point(71, 12);
			this.txtSyntax.Name = "txtSyntax";
			this.txtSyntax.Size = new System.Drawing.Size(344, 20);
			this.txtSyntax.TabIndex = 0;
			this.txtSyntax.Validating += new System.ComponentModel.CancelEventHandler(this.txtSyntax_Validating);
			this.txtSyntax.TextChanged += new System.EventHandler(this.MemberChanged);
			// 
			// lblSyntax
			// 
			this.lblSyntax.Location = new System.Drawing.Point(0, 15);
			this.lblSyntax.Name = "lblSyntax";
			this.lblSyntax.Size = new System.Drawing.Size(65, 13);
			this.lblSyntax.TabIndex = 10;
			this.lblSyntax.Text = "Syntax";
			this.lblSyntax.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblName
			// 
			this.lblName.Location = new System.Drawing.Point(0, 41);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(65, 13);
			this.lblName.TabIndex = 11;
			this.lblName.Text = "Name";
			this.lblName.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// txtName
			// 
			this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.txtName.Location = new System.Drawing.Point(71, 38);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(216, 20);
			this.txtName.TabIndex = 1;
			this.txtName.Validating += new System.ComponentModel.CancelEventHandler(this.txtName_Validating);
			this.txtName.TextChanged += new System.EventHandler(this.MemberChanged);
			// 
			// lblType
			// 
			this.lblType.Location = new System.Drawing.Point(0, 67);
			this.lblType.Name = "lblType";
			this.lblType.Size = new System.Drawing.Size(65, 13);
			this.lblType.TabIndex = 12;
			this.lblType.Text = "Type";
			this.lblType.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblAccess
			// 
			this.lblAccess.Location = new System.Drawing.Point(0, 94);
			this.lblAccess.Name = "lblAccess";
			this.lblAccess.Size = new System.Drawing.Size(65, 13);
			this.lblAccess.TabIndex = 13;
			this.lblAccess.Text = "Access";
			this.lblAccess.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// cboAccess
			// 
			this.cboAccess.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.cboAccess.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboAccess.FormattingEnabled = true;
			this.cboAccess.Items.AddRange(new object[] {
            "Default",
            "Public",
            "Protected Internal",
            "Internal",
            "Protected",
            "Private"});
			this.cboAccess.Location = new System.Drawing.Point(71, 91);
			this.cboAccess.Name = "cboAccess";
			this.cboAccess.Size = new System.Drawing.Size(216, 21);
			this.cboAccess.TabIndex = 3;
			this.cboAccess.SelectedIndexChanged += new System.EventHandler(this.cboAccess_SelectedIndexChanged);
			// 
			// chkStatic
			// 
			this.chkStatic.AutoSize = true;
			this.chkStatic.Location = new System.Drawing.Point(11, 22);
			this.chkStatic.Name = "chkStatic";
			this.chkStatic.Size = new System.Drawing.Size(53, 17);
			this.chkStatic.TabIndex = 0;
			this.chkStatic.Text = "Static";
			this.chkStatic.UseVisualStyleBackColor = true;
			this.chkStatic.CheckedChanged += new System.EventHandler(this.chkStatic_CheckedChanged);
			// 
			// chkReadonly
			// 
			this.chkReadonly.AutoSize = true;
			this.chkReadonly.Location = new System.Drawing.Point(11, 48);
			this.chkReadonly.Name = "chkReadonly";
			this.chkReadonly.Size = new System.Drawing.Size(71, 17);
			this.chkReadonly.TabIndex = 1;
			this.chkReadonly.Text = "Readonly";
			this.chkReadonly.UseVisualStyleBackColor = true;
			this.chkReadonly.CheckedChanged += new System.EventHandler(this.chkReadonly_CheckedChanged);
			// 
			// chkConst
			// 
			this.chkConst.AutoSize = true;
			this.chkConst.Location = new System.Drawing.Point(11, 74);
			this.chkConst.Name = "chkConst";
			this.chkConst.Size = new System.Drawing.Size(53, 17);
			this.chkConst.TabIndex = 2;
			this.chkConst.Text = "Const";
			this.chkConst.UseVisualStyleBackColor = true;
			this.chkConst.CheckedChanged += new System.EventHandler(this.chkConst_CheckedChanged);
			// 
			// txtInitialValue
			// 
			this.txtInitialValue.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.txtInitialValue.Location = new System.Drawing.Point(71, 145);
			this.txtInitialValue.Name = "txtInitialValue";
			this.txtInitialValue.Size = new System.Drawing.Size(216, 20);
			this.txtInitialValue.TabIndex = 5;
			this.txtInitialValue.Validating += new System.ComponentModel.CancelEventHandler(this.txtInitialValue_Validating);
			this.txtInitialValue.TextChanged += new System.EventHandler(this.MemberChanged);
			// 
			// lblInitValue
			// 
			this.lblInitValue.Location = new System.Drawing.Point(0, 148);
			this.lblInitValue.Name = "lblInitValue";
			this.lblInitValue.Size = new System.Drawing.Size(65, 13);
			this.lblInitValue.TabIndex = 15;
			this.lblInitValue.Text = "Initial value";
			this.lblInitValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lstMembers
			// 
			this.lstMembers.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.lstMembers.AutoArrange = false;
			this.lstMembers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.icon,
            this.name,
            this.type,
            this.access,
            this.modifier});
			this.lstMembers.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (238)));
			this.lstMembers.FullRowSelect = true;
			this.lstMembers.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.lstMembers.HideSelection = false;
			this.lstMembers.Location = new System.Drawing.Point(12, 208);
			this.lstMembers.MultiSelect = false;
			this.lstMembers.Name = "lstMembers";
			this.lstMembers.ShowGroups = false;
			this.lstMembers.Size = new System.Drawing.Size(422, 246);
			this.lstMembers.TabIndex = 8;
			this.lstMembers.UseCompatibleStateImageBehavior = false;
			this.lstMembers.View = System.Windows.Forms.View.Details;
			this.lstMembers.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstMembers_KeyDown);
			this.lstMembers.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lstMembers_ItemSelectionChanged);
			// 
			// icon
			// 
			this.icon.Text = "";
			this.icon.Width = 20;
			// 
			// name
			// 
			this.name.Text = "Name";
			this.name.Width = 119;
			// 
			// type
			// 
			this.type.Text = "Type";
			this.type.Width = 101;
			// 
			// access
			// 
			this.access.Text = "Access";
			this.access.Width = 102;
			// 
			// modifier
			// 
			this.modifier.Text = "Modifier";
			// 
			// grpOtherModifiers
			// 
			this.grpOtherModifiers.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.grpOtherModifiers.Controls.Add(this.chkNew);
			this.grpOtherModifiers.Controls.Add(this.chkConst);
			this.grpOtherModifiers.Controls.Add(this.chkReadonly);
			this.grpOtherModifiers.Controls.Add(this.chkStatic);
			this.grpOtherModifiers.Location = new System.Drawing.Point(307, 38);
			this.grpOtherModifiers.Name = "grpOtherModifiers";
			this.grpOtherModifiers.Size = new System.Drawing.Size(127, 127);
			this.grpOtherModifiers.TabIndex = 6;
			this.grpOtherModifiers.TabStop = false;
			this.grpOtherModifiers.Text = "Egyéb módosítók";
			// 
			// chkNew
			// 
			this.chkNew.AutoSize = true;
			this.chkNew.Enabled = false;
			this.chkNew.Location = new System.Drawing.Point(11, 100);
			this.chkNew.Name = "chkNew";
			this.chkNew.Size = new System.Drawing.Size(106, 17);
			this.chkNew.TabIndex = 3;
			this.chkNew.Text = "New (planning...)";
			this.chkNew.UseVisualStyleBackColor = true;
			// 
			// cboType
			// 
			this.cboType.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.cboType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
			this.cboType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
			this.cboType.FormattingEnabled = true;
			this.cboType.Location = new System.Drawing.Point(71, 64);
			this.cboType.Name = "cboType";
			this.cboType.Size = new System.Drawing.Size(216, 21);
			this.cboType.Sorted = true;
			this.cboType.TabIndex = 2;
			this.cboType.Validating += new System.ComponentModel.CancelEventHandler(this.cboType_Validating);
			this.cboType.TextChanged += new System.EventHandler(this.MemberChanged);
			// 
			// cboModifier
			// 
			this.cboModifier.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.cboModifier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboModifier.FormattingEnabled = true;
			this.cboModifier.Items.AddRange(new object[] {
            "Default",
            "Public",
            "Protected Internal",
            "Internal",
            "Protected",
            "Private"});
			this.cboModifier.Location = new System.Drawing.Point(71, 118);
			this.cboModifier.Name = "cboModifier";
			this.cboModifier.Size = new System.Drawing.Size(216, 21);
			this.cboModifier.TabIndex = 4;
			this.cboModifier.SelectedIndexChanged += new System.EventHandler(this.cboModifier_SelectedIndexChanged);
			// 
			// lblModifier
			// 
			this.lblModifier.Location = new System.Drawing.Point(0, 121);
			this.lblModifier.Name = "lblModifier";
			this.lblModifier.Size = new System.Drawing.Size(65, 13);
			this.lblModifier.TabIndex = 14;
			this.lblModifier.Text = "Modifier";
			this.lblModifier.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// btnClose
			// 
			this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClose.Location = new System.Drawing.Point(359, 460);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 23);
			this.btnClose.TabIndex = 9;
			this.btnClose.Text = "Bezár";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// toolStrip
			// 
			this.toolStrip.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.toolStrip.AutoSize = false;
			this.toolStrip.BackColor = System.Drawing.SystemColors.Control;
			this.toolStrip.Dock = System.Windows.Forms.DockStyle.None;
			this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolNewField,
            this.toolNewMethod,
            this.toolNewConstructor,
            this.toolNewDestructor,
            this.toolNewProperty,
            this.toolNewEvent,
            this.toolDelete,
            this.toolSepMoving,
            this.toolMoveDown,
            this.toolMoveUp,
            this.toolSepSorting,
            this.toolSortByName,
            this.toolSortByAccess,
            this.toolSortByKind,
            this.toolSepAddNew,
            this.toolOverrideList,
            this.toolImplementList});
			this.toolStrip.Location = new System.Drawing.Point(12, 180);
			this.toolStrip.Name = "toolStrip";
			this.toolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
			this.toolStrip.Size = new System.Drawing.Size(422, 25);
			this.toolStrip.TabIndex = 7;
			this.toolStrip.Text = "toolStrip1";
			// 
			// toolNewField
			// 
			this.toolNewField.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolNewField.Image = global::NClass.GUI.Diagram.Properties.Resources.Field;
			this.toolNewField.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolNewField.Name = "toolNewField";
			this.toolNewField.Size = new System.Drawing.Size(23, 22);
			this.toolNewField.Text = "New Field";
			this.toolNewField.Click += new System.EventHandler(this.toolNewField_Click);
			// 
			// toolNewMethod
			// 
			this.toolNewMethod.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolNewMethod.Image = global::NClass.GUI.Diagram.Properties.Resources.Method;
			this.toolNewMethod.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolNewMethod.Name = "toolNewMethod";
			this.toolNewMethod.Size = new System.Drawing.Size(23, 22);
			this.toolNewMethod.Text = "New Method";
			this.toolNewMethod.Click += new System.EventHandler(this.toolNewMethod_Click);
			// 
			// toolNewConstructor
			// 
			this.toolNewConstructor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolNewConstructor.Image = global::NClass.GUI.Diagram.Properties.Resources.Constructor;
			this.toolNewConstructor.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolNewConstructor.Name = "toolNewConstructor";
			this.toolNewConstructor.Size = new System.Drawing.Size(23, 22);
			this.toolNewConstructor.Text = "New Constructor";
			this.toolNewConstructor.Click += new System.EventHandler(this.toolNewConstructor_Click);
			// 
			// toolNewDestructor
			// 
			this.toolNewDestructor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolNewDestructor.Image = global::NClass.GUI.Diagram.Properties.Resources.Destructor;
			this.toolNewDestructor.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolNewDestructor.Name = "toolNewDestructor";
			this.toolNewDestructor.Size = new System.Drawing.Size(23, 22);
			this.toolNewDestructor.Text = "New Destructor";
			this.toolNewDestructor.Click += new System.EventHandler(this.toolNewDestructor_Click);
			// 
			// toolNewProperty
			// 
			this.toolNewProperty.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolNewProperty.Image = global::NClass.GUI.Diagram.Properties.Resources.Property;
			this.toolNewProperty.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolNewProperty.Name = "toolNewProperty";
			this.toolNewProperty.Size = new System.Drawing.Size(23, 22);
			this.toolNewProperty.Text = "New Property";
			this.toolNewProperty.Click += new System.EventHandler(this.toolNewProperty_Click);
			// 
			// toolNewEvent
			// 
			this.toolNewEvent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolNewEvent.Image = global::NClass.GUI.Diagram.Properties.Resources.Event;
			this.toolNewEvent.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolNewEvent.Name = "toolNewEvent";
			this.toolNewEvent.Size = new System.Drawing.Size(23, 22);
			this.toolNewEvent.Text = "New Event";
			this.toolNewEvent.Click += new System.EventHandler(this.toolNewEvent_Click);
			// 
			// toolDelete
			// 
			this.toolDelete.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.toolDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolDelete.Image = global::NClass.GUI.Diagram.Properties.Resources.Delete;
			this.toolDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolDelete.Name = "toolDelete";
			this.toolDelete.Size = new System.Drawing.Size(23, 22);
			this.toolDelete.Text = "Delete";
			this.toolDelete.Click += new System.EventHandler(this.toolDelete_Click);
			// 
			// toolSepMoving
			// 
			this.toolSepMoving.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.toolSepMoving.Name = "toolSepMoving";
			this.toolSepMoving.Size = new System.Drawing.Size(6, 25);
			// 
			// toolMoveDown
			// 
			this.toolMoveDown.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.toolMoveDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolMoveDown.Image = ((System.Drawing.Image) (resources.GetObject("toolMoveDown.Image")));
			this.toolMoveDown.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolMoveDown.Name = "toolMoveDown";
			this.toolMoveDown.Size = new System.Drawing.Size(23, 22);
			this.toolMoveDown.Text = "Move Down";
			this.toolMoveDown.Click += new System.EventHandler(this.toolMoveDown_Click);
			// 
			// toolMoveUp
			// 
			this.toolMoveUp.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.toolMoveUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolMoveUp.Image = ((System.Drawing.Image) (resources.GetObject("toolMoveUp.Image")));
			this.toolMoveUp.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolMoveUp.Name = "toolMoveUp";
			this.toolMoveUp.Size = new System.Drawing.Size(23, 22);
			this.toolMoveUp.Text = "Move Up";
			this.toolMoveUp.Click += new System.EventHandler(this.toolMoveUp_Click);
			// 
			// toolSepSorting
			// 
			this.toolSepSorting.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.toolSepSorting.Name = "toolSepSorting";
			this.toolSepSorting.Size = new System.Drawing.Size(6, 25);
			// 
			// toolSortByName
			// 
			this.toolSortByName.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.toolSortByName.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolSortByName.Image = global::NClass.GUI.Diagram.Properties.Resources.SortByName;
			this.toolSortByName.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolSortByName.Name = "toolSortByName";
			this.toolSortByName.Size = new System.Drawing.Size(23, 22);
			this.toolSortByName.Text = "Sort by Name";
			this.toolSortByName.Click += new System.EventHandler(this.toolSortByName_Click);
			// 
			// toolSortByAccess
			// 
			this.toolSortByAccess.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.toolSortByAccess.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolSortByAccess.Image = global::NClass.GUI.Diagram.Properties.Resources.SortByAccess;
			this.toolSortByAccess.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolSortByAccess.Name = "toolSortByAccess";
			this.toolSortByAccess.Size = new System.Drawing.Size(23, 22);
			this.toolSortByAccess.Text = "Sort by Access";
			this.toolSortByAccess.Click += new System.EventHandler(this.toolSortByAccess_Click);
			// 
			// toolSortByKind
			// 
			this.toolSortByKind.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.toolSortByKind.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolSortByKind.Image = global::NClass.GUI.Diagram.Properties.Resources.SortByKind;
			this.toolSortByKind.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolSortByKind.Name = "toolSortByKind";
			this.toolSortByKind.Size = new System.Drawing.Size(23, 22);
			this.toolSortByKind.Text = "Sort by Kind";
			this.toolSortByKind.Click += new System.EventHandler(this.toolSortByKind_Click);
			// 
			// toolSepAddNew
			// 
			this.toolSepAddNew.Name = "toolSepAddNew";
			this.toolSepAddNew.Size = new System.Drawing.Size(6, 25);
			// 
			// toolOverrideList
			// 
			this.toolOverrideList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolOverrideList.Image = global::NClass.GUI.Diagram.Properties.Resources.Overrides;
			this.toolOverrideList.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolOverrideList.Name = "toolOverrideList";
			this.toolOverrideList.Size = new System.Drawing.Size(23, 22);
			this.toolOverrideList.Text = "Override List";
			this.toolOverrideList.Click += new System.EventHandler(this.toolOverrideList_Click);
			// 
			// toolImplementList
			// 
			this.toolImplementList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolImplementList.Image = global::NClass.GUI.Diagram.Properties.Resources.Implements;
			this.toolImplementList.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolImplementList.Name = "toolImplementList";
			this.toolImplementList.Size = new System.Drawing.Size(23, 22);
			this.toolImplementList.Text = "Interface Implement List";
			this.toolImplementList.Click += new System.EventHandler(this.toolImplementList_Click);
			// 
			// MembersDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CausesValidation = false;
			this.ClientSize = new System.Drawing.Size(446, 495);
			this.Controls.Add(this.toolStrip);
			this.Controls.Add(this.lstMembers);
			this.Controls.Add(this.btnClose);
			this.Controls.Add(this.cboModifier);
			this.Controls.Add(this.lblModifier);
			this.Controls.Add(this.cboType);
			this.Controls.Add(this.grpOtherModifiers);
			this.Controls.Add(this.txtInitialValue);
			this.Controls.Add(this.lblInitValue);
			this.Controls.Add(this.cboAccess);
			this.Controls.Add(this.lblAccess);
			this.Controls.Add(this.lblType);
			this.Controls.Add(this.txtName);
			this.Controls.Add(this.lblName);
			this.Controls.Add(this.lblSyntax);
			this.Controls.Add(this.txtSyntax);
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(400, 450);
			this.Name = "MembersDialog";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Members";
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PropertiesDialog_KeyDown);
			((System.ComponentModel.ISupportInitialize) (this.errorProvider)).EndInit();
			this.grpOtherModifiers.ResumeLayout(false);
			this.grpOtherModifiers.PerformLayout();
			this.toolStrip.ResumeLayout(false);
			this.toolStrip.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ErrorProvider errorProvider;
		private System.Windows.Forms.Label lblSyntax;
		private System.Windows.Forms.TextBox txtSyntax;
		private System.Windows.Forms.ComboBox cboAccess;
		private System.Windows.Forms.Label lblAccess;
		private System.Windows.Forms.Label lblType;
		private System.Windows.Forms.TextBox txtName;
		private System.Windows.Forms.Label lblName;
		private System.Windows.Forms.TextBox txtInitialValue;
		private System.Windows.Forms.Label lblInitValue;
		private System.Windows.Forms.CheckBox chkConst;
		private System.Windows.Forms.CheckBox chkReadonly;
		private System.Windows.Forms.CheckBox chkStatic;
		private System.Windows.Forms.ListView lstMembers;
		private System.Windows.Forms.ColumnHeader name;
		private System.Windows.Forms.ColumnHeader type;
		private System.Windows.Forms.ColumnHeader access;
		private System.Windows.Forms.GroupBox grpOtherModifiers;
		private System.Windows.Forms.ComboBox cboType;
		private System.Windows.Forms.ComboBox cboModifier;
		private System.Windows.Forms.Label lblModifier;
		private System.Windows.Forms.ColumnHeader icon;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.ColumnHeader modifier;
		private System.Windows.Forms.ToolStrip toolStrip;
		private System.Windows.Forms.ToolStripButton toolNewField;
		private System.Windows.Forms.ToolStripButton toolNewMethod;
		private System.Windows.Forms.ToolStripButton toolNewProperty;
		private System.Windows.Forms.ToolStripButton toolNewConstructor;
		private System.Windows.Forms.ToolStripButton toolNewDestructor;
		private System.Windows.Forms.ToolStripButton toolNewEvent;
		private System.Windows.Forms.ToolStripButton toolDelete;
		private System.Windows.Forms.ToolStripButton toolMoveUp;
		private System.Windows.Forms.ToolStripButton toolMoveDown;
		private System.Windows.Forms.ToolStripButton toolOverrideList;
		private System.Windows.Forms.ToolStripButton toolImplementList;
		private System.Windows.Forms.ToolStripSeparator toolSepMoving;
		private System.Windows.Forms.ToolStripSeparator toolSepSorting;
		private System.Windows.Forms.ToolStripButton toolSortByName;
		private System.Windows.Forms.ToolStripButton toolSortByAccess;
		private System.Windows.Forms.ToolStripButton toolSortByKind;
		private System.Windows.Forms.ToolStripSeparator toolSepAddNew;
		private System.Windows.Forms.CheckBox chkNew;
	}
}

