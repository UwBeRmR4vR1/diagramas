﻿// NClass - UML class diagram editor
// Copyright (C) 2006 Balázs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using NClass.Core;
using NClass.Translations;

namespace NClass.GUI.Diagram
{
	public partial class OverrideDialog : Form
	{
		public OverrideDialog()
		{
			InitializeComponent();
			lstMembers.SmallImageList = Icons.IconList;
		}

		public IEnumerable<Operation> SelectedOperations
		{
			get
			{
				for (int i = 0; i < lstMembers.Items.Count; i++) {
					if (lstMembers.Items[i].Checked && lstMembers.Items[i].Tag is Operation)
						yield return (Operation) lstMembers.Items[i].Tag;
				}
			}
		}

		private void UpdateTexts()
		{
			this.Text = Texts.GetText("override_members");
			lstMembers.Columns[0].Text = Texts.GetText("name");
			btnOK.Text = Texts.GetText("button_ok");
			btnCancel.Text = Texts.GetText("button_cancel");
		}

		private void AddMembers(ClassType baseClass)
		{
			if (baseClass == null)
				return;

			foreach (Operation operation in baseClass.OverridableOperations) {
				ListViewItem item = lstMembers.Items.Add(operation.GetCaption());

				item.Tag = operation;
				item.ImageIndex = Icons.GetImageIndex(operation);
				item.ToolTipText = operation.ToString();
			}

			AddMembers(baseClass.Base);
		}

		public DialogResult ShowDialog(ClassType inheritedClass)
		{
			if (inheritedClass == null)
				return DialogResult.None;

			lstMembers.Items.Clear();
			AddMembers(inheritedClass.Base);

			return ShowDialog();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			UpdateTexts();
		}
	}
}