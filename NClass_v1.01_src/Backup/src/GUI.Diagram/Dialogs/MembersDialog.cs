﻿// NClass - UML class diagram editor
// Copyright (C) 2006 Balázs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Windows.Forms;
using NClass.Core;
using NClass.Translations;

namespace NClass.GUI.Diagram
{
	public partial class MembersDialog : Form
	{
		OperationContainer parent = null;
		Member member = null;
		bool locked = false;
		int attributeCount = 0;
		AccessModifier[] accessOrder = null;
		OperationModifier[] modifierOrder = null;
		bool changed = false;

		#region Enum orders

		static AccessModifier[] csharpAccessOrder = {
			AccessModifier.Public,
			AccessModifier.ProtectedInternal,
			AccessModifier.Internal,
			AccessModifier.Protected,
			AccessModifier.Private,
			AccessModifier.Default
		};

		static AccessModifier[] javaAccessOrder = {
			AccessModifier.Public,
			AccessModifier.Protected,
			AccessModifier.Private,
			AccessModifier.Default
		};

		static OperationModifier[] csharpModifierOrder = {
			OperationModifier.None,
			OperationModifier.Virtual,
			OperationModifier.Abstract,
			OperationModifier.Override,
			OperationModifier.Sealed
		};

		static OperationModifier[] javaModifierOrder = {
			OperationModifier.None,
			OperationModifier.Abstract,
			OperationModifier.Sealed
		};

		#endregion

		public MembersDialog()
		{
			InitializeComponent();
			lstMembers.SmallImageList = Icons.IconList;
		}

		public bool Changed
		{
			get { return changed; }
			private set { changed = value; }
		}

		private void UpdateTexts()
		{
			lblSyntax.Text = Texts.GetText("syntax");
			lblName.Text = Texts.GetText("name");
			lblType.Text = Texts.GetText("type");
			lblAccess.Text = Texts.GetText("access");
			lblModifier.Text = Texts.GetText("modifier");
			lblInitValue.Text = Texts.GetText("initial_value");
			grpOtherModifiers.Text = Texts.GetText("other_modifiers");
			toolNewField.Text = Texts.GetText("new_field");
			toolNewMethod.Text = Texts.GetText("new_method");
			toolNewConstructor.Text = Texts.GetText("new_constructor");
			toolNewDestructor.Text = Texts.GetText("new_destructor");
			toolNewProperty.Text = Texts.GetText("new_property");
			toolNewEvent.Text = Texts.GetText("new_event");
			toolOverrideList.Text = Texts.GetText("override_members");
			toolImplementList.Text = Texts.GetText("implement_interfaces");
			toolSortByKind.Text = Texts.GetText("sort_by_kind");
			toolSortByAccess.Text = Texts.GetText("sort_by_access");
			toolSortByName.Text = Texts.GetText("sort_by_name");
			toolMoveUp.Text = Texts.GetText("move_up");
			toolMoveDown.Text = Texts.GetText("move_down");
			toolDelete.Text = Texts.GetText("delete");
			lstMembers.Columns[1].Text = Texts.GetText("name");
			lstMembers.Columns[2].Text = Texts.GetText("type");
			lstMembers.Columns[3].Text = Texts.GetText("access");
			lstMembers.Columns[4].Text = Texts.GetText("modifier");
			btnClose.Text = Texts.GetText("button_close");
		}

		public void ShowDialog(OperationContainer parent)
		{
			if (parent == null)
				return;

			this.parent = parent;
			this.Text = Texts.GetText("members_of_type", parent.Name);
			Changed = false;

			FillCollections(parent.Language);
			FillMembersList();
			if (lstMembers.Items.Count > 0) {
				lstMembers.Items[0].Focused = true;
				lstMembers.Items[0].Selected = true;
			}

			toolNewField.Visible = (parent is IFieldAllower);
			toolNewConstructor.Visible = (parent is IConstructorAllower);
			toolNewConstructor.Enabled = (parent is IConstructorAllower) &&
				((IConstructorAllower) parent).CanAddConstructor;
			toolNewDestructor.Visible = (parent is IDestructorAllower);
			toolNewDestructor.Enabled = (parent is IDestructorAllower) &&
				((IDestructorAllower) parent).CanAddDestructor;
			toolNewProperty.Visible = (parent is IPropertyAllower);
			toolNewEvent.Visible = (parent is IEventAllower);
			toolOverrideList.Visible = parent is IOverridableType;
			toolOverrideList.Enabled = (parent is IOverridableType) &&
				((IOverridableType) parent).HasBase;
			toolImplementList.Visible = parent is IInterfaceImplementer;
			toolImplementList.Enabled = (parent is IInterfaceImplementer) &&
				((IInterfaceImplementer) parent).ImplementsInterface;
			toolSepAddNew.Visible = parent is IOverridableType ||
				parent is IInterfaceImplementer;

			if (parent.Language == Language.CSharp) {
				accessOrder = csharpAccessOrder;
				modifierOrder = csharpModifierOrder;
			}
			else {
				accessOrder = javaAccessOrder;
				modifierOrder = javaModifierOrder;
			}

			errorProvider.SetError(txtSyntax, null);
			errorProvider.SetError(txtName, null);
			errorProvider.SetError(cboType, null);

			base.ShowDialog();
		}

		private void FillCollections(Language language)
		{
			cboAccess.Items.Clear();
			cboModifier.Items.Clear();

			if (language == Language.CSharp) {
				cboAccess.Items.Add("Public");
				cboAccess.Items.Add("Protected Internal");
				cboAccess.Items.Add("Internal");
				cboAccess.Items.Add("Protected");
				cboAccess.Items.Add("Private");
				cboAccess.Items.Add("Default");

				cboModifier.Items.Add("None");
				cboModifier.Items.Add("Virtual");
				cboModifier.Items.Add("Abstract");
				cboModifier.Items.Add("Override");
				cboModifier.Items.Add("Sealed");
			}
			else {
				cboAccess.Items.Add("Public");
				cboAccess.Items.Add("Protected");
				cboAccess.Items.Add("Private");
				cboAccess.Items.Add("Default");

				cboModifier.Items.Add("None");
				cboModifier.Items.Add("Abstract");
				cboModifier.Items.Add("Final");
			}
		}

		private void FillMembersList()
		{
			lstMembers.Items.Clear();
			attributeCount = 0;

			if (parent is IFieldAllower) {
				foreach (Field field in ((IFieldAllower) parent).Fields)
					AddFieldToList(field);
			}

			foreach (Operation operation in parent.Operations)
				AddOperationToList(operation);

			DisableFields();
		}

		private ListViewItem AddFieldToList(Field field)
		{
			ListViewItem item = lstMembers.Items.Insert(attributeCount, "");

			item.Tag = field;
			item.ImageIndex = Icons.GetImageIndex(field);
			item.SubItems.Add(field.Name);
			item.SubItems.Add(field.Type);
			item.SubItems.Add(SyntaxHelper.GetAccessModifier(
				field.AccessModifier, parent.Language, false));
			item.SubItems.Add("");
			attributeCount++;

			return item;
		}

		private ListViewItem AddOperationToList(Operation operation)
		{
			ListViewItem item = lstMembers.Items.Add("");

			item.Tag = operation;
			item.ImageIndex = Icons.GetImageIndex(operation);
			item.SubItems.Add(operation.Name);
			item.SubItems.Add(operation.Type);
			item.SubItems.Add(SyntaxHelper.GetAccessModifier(
				operation.AccessModifier, parent.Language, false));
			item.SubItems.Add(SyntaxHelper.GetOperationModifier(
				operation.Modifier, parent.Language, false));

			return item;
		}

		private void ShowNewMember(Member actualMember)
		{
			if (locked || actualMember == null)
				return;
			else
				member = actualMember;

			RefreshValues();
		}

		private void RefreshValues()
		{
			if (member == null)
				return;

			locked = true;
			
			txtSyntax.Enabled = true;
			txtName.Enabled = true;
			txtSyntax.ReadOnly = (member is Destructor);
			txtName.ReadOnly = (member == null || member.IsNameReadonly);
			cboType.Enabled = (member != null && !member.IsTypeReadonly);
			cboAccess.Enabled = (member != null && member.CanSetAccess);
			cboModifier.Enabled = (member is Operation && ((Operation) member).CanSetModifier);
			txtInitialValue.Enabled = (member is Field);
			chkStatic.Enabled = (member != null && member.CanSetStatic);
			chkReadonly.Enabled = (member is Field);
			chkConst.Enabled = (member is IConst && ((IConst) member).CanSetConst);
			toolSortByKind.Enabled = true;
			toolSortByAccess.Enabled = true;
			toolSortByName.Enabled = true;

			if (lstMembers.Items.Count > 0) {
				toolSortByKind.Enabled = true;
				toolSortByAccess.Enabled = true;
				toolSortByName.Enabled = true;
			}

			txtSyntax.Text = member.ToString();
			txtName.Text = member.Name;
			cboType.Text = member.Type;

			// Access selection
			for (int i = 0; i < accessOrder.Length; i++) {
				if (accessOrder[i] == member.AccessModifier) {
					cboAccess.SelectedIndex = i;
					break;
				}
			}

			// Modifier selection
			if (member is Operation) {
				for (int i = 0; i < modifierOrder.Length; i++) {
					if (modifierOrder[i] == ((Operation) member).Modifier) {
						cboModifier.SelectedIndex = i;
						break;
					}
				}
			}
			else {
				cboModifier.Text = null;
			}

			chkStatic.Checked = member.IsStatic;
			if (member is Field) {
				chkReadonly.Checked = ((Field) member).IsReadonly;
				txtInitialValue.Text = ((Field) member).InitialValue;
			}
			else {
				chkReadonly.Checked = false;
				txtInitialValue.Text = null;
			}

			// Contant verifying
			if (member is IConst)
				chkConst.Checked = ((IConst) member).IsConst;
			else
				chkConst.Checked = false;

			RefreshMembersList();
			
			locked = false;

			errorProvider.SetError(txtSyntax, null);
			errorProvider.SetError(txtName, null);
			errorProvider.SetError(cboType, null);
		}

		private void RefreshMembersList()
		{
			ListViewItem item = null;

			if (lstMembers.FocusedItem != null)
				item = lstMembers.FocusedItem;
			else if (lstMembers.SelectedItems.Count > 0)
				item = lstMembers.SelectedItems[0];

			if (item != null && member != null) {
				item.ImageIndex = Icons.GetImageIndex(member);
				item.SubItems[1].Text = txtName.Text;
				item.SubItems[2].Text = cboType.Text;
				item.SubItems[3].Text = cboAccess.Text;
				item.SubItems[4].Text = cboModifier.Text;
			}
		}

		private void DisableFields()
		{
			member = null;

			locked = true;

			txtSyntax.Text = null;
			txtName.Text = null;
			cboType.Text = null;
			cboAccess.Text = null;
			cboModifier.Text = null;
			txtInitialValue.Text = null;
			chkStatic.Checked = false;
			chkReadonly.Checked = false;
			chkConst.Checked = false;

			txtSyntax.Enabled = false;
			txtName.Enabled = false;
			cboType.Enabled = false;
			cboAccess.Enabled = false;
			cboModifier.Enabled = false;
			txtInitialValue.Enabled = false;
			chkStatic.Enabled = false;
			chkReadonly.Enabled = false;
			chkConst.Enabled = false;

			if (lstMembers.Items.Count == 0) {
				toolSortByKind.Enabled = false;
				toolSortByAccess.Enabled = false;
				toolSortByName.Enabled = false;
			}
			toolMoveUp.Enabled = false;
			toolMoveDown.Enabled = false;
			toolDelete.Enabled = false;

			locked = false;
		}

		private void SwapListItems(ListViewItem item1, ListViewItem item2)
		{
			int image = item1.ImageIndex;
			item1.ImageIndex = item2.ImageIndex;
			item2.ImageIndex = image;

			object tag = item1.Tag;
			item1.Tag = item2.Tag;
			item2.Tag = tag;

			for (int i = 0; i < item1.SubItems.Count; i++) {
				string text = item1.SubItems[i].Text;
				item1.SubItems[i].Text = item2.SubItems[i].Text;
				item2.SubItems[i].Text = text;
			}
			Changed = true;
		}

		private void DeleteSelectedMember()
		{
			if (lstMembers.SelectedItems.Count > 0) {
				ListViewItem item = lstMembers.SelectedItems[0];
				int index = item.Index;

				if (item.Tag is Field)
					attributeCount--;
				parent.RemoveMember(item.Tag as Member);
				lstMembers.Items.Remove(item);
				Changed = true;

				int count = lstMembers.Items.Count;
				if (count > 0) {
					if (index >= count)
						index = count - 1;
					lstMembers.Items[index].Selected = true;
				}
				else {
					DisableFields();
				}
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			UpdateTexts();
		}

		private void PropertiesDialog_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Escape)
				RefreshValues();
			else if (e.KeyCode == Keys.Enter)
				lstMembers.Focus();
		}

		private void txtSyntax_Validating(object sender, CancelEventArgs e)
		{
			if (!locked && member != null) {
				try {
					member.InitFromString(txtSyntax.Text);
					errorProvider.SetError(txtSyntax, null);
					RefreshValues();
				}
				catch (BadSyntaxException ex) {
					e.Cancel = true;
					errorProvider.SetError(txtSyntax, ex.Message);
				}
			}
		}

		private void txtName_Validating(object sender, CancelEventArgs e)
		{
			if (!locked && member != null) {
				try {
					member.Name = txtName.Text;
					errorProvider.SetError(txtName, null);
					RefreshValues();
				}
				catch (BadSyntaxException ex) {
					e.Cancel = true;
					errorProvider.SetError(txtName, ex.Message);
				}
			}
		}

		private void cboType_Validating(object sender, CancelEventArgs e)
		{
			if (!locked && member != null) {
				try {
					member.Type = cboType.Text;
					if (!cboType.Items.Contains(cboType.Text))
						cboType.Items.Add(cboType.Text);
					errorProvider.SetError(cboType, null);
					cboType.Select(0, 0);
					RefreshValues();
				}
				catch (BadSyntaxException ex) {
					e.Cancel = true;
					errorProvider.SetError(cboType, ex.Message);
				}
			}
		}

		private void cboAccess_SelectedIndexChanged(object sender, EventArgs e)
		{
			int index = cboAccess.SelectedIndex;

			if (!locked && member != null) {
				try {
					member.AccessModifier = accessOrder[index];
					RefreshValues();
					Changed = true;
				}
				catch {
					// Skips
				}
			}
		}

		private void cboModifier_SelectedIndexChanged(object sender, EventArgs e)
		{
			int index = cboModifier.SelectedIndex;

			if (!locked && member is Operation) {
				try {
					if (modifierOrder[index] == OperationModifier.Abstract &&
						((ClassType) parent).Modifier != InheritanceModifier.Abstract)
					{
						DialogResult result = MessageBox.Show(
							Texts.GetText("abstract_changing"), Texts.GetText("confirmation"),
							MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

						if (result == DialogResult.No) {
							RefreshValues();
							return;
						}
					}

					((Operation) member).Modifier = modifierOrder[index];
					RefreshValues();
					Changed = true;
				}
				catch {
					// Skips
				}
			}
		}

		private void txtInitialValue_Validating(object sender, CancelEventArgs e)
		{
			if (!locked && member is Field) {
				if (txtInitialValue.Text != "" && txtInitialValue.Text[0] == '"' &&
					!txtInitialValue.Text.EndsWith("\""))
				{
					txtInitialValue.Text += '"';
				}
				((Field) member).InitialValue = txtInitialValue.Text;
				RefreshValues();
			}
		}

		private void MemberChanged(object sender, EventArgs e)
		{
			if (!locked)
				Changed = true;
		}

		private void chkStatic_CheckedChanged(object sender, EventArgs e)
		{
			if (!locked && member != null) {
				try {
					member.IsStatic = chkStatic.Checked;
					RefreshValues();
					Changed = true;
				}
				catch  {
					// Skips
				}
			}
		}

		private void chkReadonly_CheckedChanged(object sender, EventArgs e)
		{
			if (!locked && member is Field) {
				try {
					((Field) member).IsReadonly = chkReadonly.Checked;
					RefreshValues();
					Changed = true;
				}
				catch {
					// Skips
				}
			}
		}

		private void chkConst_CheckedChanged(object sender, EventArgs e)
		{
			if (!locked && member is IConst) {
				try {
					((IConst) member).IsConst = chkConst.Checked;
					if (!chkConst.Checked && member is Field) {
						member.IsStatic = false;
						((Field) member).IsReadonly = false;
					}
					RefreshValues();
					Changed = true;
				}
				catch {
					// Skips
				}
			}
		}

		private void lstMembers_ItemSelectionChanged(object sender,
			ListViewItemSelectionChangedEventArgs e)
		{
			if (e.IsSelected && e.Item.Tag is Member) {
				ShowNewMember((Member) e.Item.Tag);

				toolDelete.Enabled = true;
				if (e.ItemIndex < attributeCount) {
					toolMoveUp.Enabled = (e.ItemIndex > 0);
					toolMoveDown.Enabled = (e.ItemIndex < attributeCount - 1);
				}
				else {
					toolMoveUp.Enabled = (e.ItemIndex > attributeCount);
					toolMoveDown.Enabled = (e.ItemIndex < lstMembers.Items.Count - 1);
				}
			}
			else {
				toolMoveUp.Enabled = false;
				toolMoveDown.Enabled = false;
				toolDelete.Enabled = false;
			}
		}

		private void lstMembers_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
				DeleteSelectedMember();
		}

		private void toolNewField_Click(object sender, EventArgs e)
		{
			if (parent is IFieldAllower) {
				Field field = ((IFieldAllower) parent).AddField("NewField");
				ListViewItem item = AddFieldToList(field);

				Changed = true;
				item.Focused = true;
				item.Selected = true;
				txtName.SelectAll();
				txtName.Focus();
			}
		}

		private void toolNewMethod_Click(object sender, EventArgs e)
		{
			Method method = parent.AddMethod("NewMethod");
			ListViewItem item = AddOperationToList(method);

			Changed = true;
			item.Focused = true;
			item.Selected = true;
			txtName.SelectAll();
			txtName.Focus();
		}

		private void toolNewProperty_Click(object sender, EventArgs e)
		{
			if (parent is IPropertyAllower) {
				Property property = ((IPropertyAllower) parent).AddProperty("NewProperty");
				ListViewItem item = AddOperationToList(property);

				Changed = true;
				item.Focused = true;
				item.Selected = true;
				txtName.SelectAll();
				txtName.Focus();
			}
		}

		private void toolNewEvent_Click(object sender, EventArgs e)
		{
			if (parent is IEventAllower) {
				Event _event = ((IEventAllower) parent).AddEvent("NewEvent");
				ListViewItem item = AddOperationToList(_event);

				Changed = true;
				item.Focused = true;
				item.Selected = true;
				txtName.SelectAll();
				txtName.Focus();
			}
		}

		private void toolNewConstructor_Click(object sender, EventArgs e)
		{
			if (parent is IConstructorAllower) {
				Method constructor = ((IConstructorAllower) parent).AddConstructor();
				ListViewItem item = AddOperationToList(constructor);

				Changed = true;
				item.Focused = true;
				item.Selected = true;
			}
		}

		private void toolNewDestructor_Click(object sender, EventArgs e)
		{
			if (parent is IDestructorAllower) {
				Method destructor = ((IDestructorAllower) parent).AddDestructor();
				ListViewItem item = AddOperationToList(destructor);

				Changed = true;
				item.Focused = true;
				item.Selected = true;
			}
		}

		private void toolOverrideList_Click(object sender, EventArgs e)
		{
			if (parent is ClassType) {
				using (OverrideDialog dialog = new OverrideDialog()) {
					if (dialog.ShowDialog(parent as ClassType) == DialogResult.OK) {
						foreach (Operation operation in dialog.SelectedOperations){
							Operation overridden = 
								((ClassType) parent).Override(operation);
							AddOperationToList(overridden);
						}
					}
				}
			}
		}

		private void toolImplementList_Click(object sender, EventArgs e)
		{
			if (parent is IInterfaceImplementer) {
				using (ImplementDialog dialog = new ImplementDialog()) {
					if (dialog.ShowDialog(parent as IInterfaceImplementer) == DialogResult.OK) {
						foreach (Operation operation in dialog.SelectedOperations) {
							Operation implemented =
								((IInterfaceImplementer) parent).Implement(operation);
							AddOperationToList(implemented);
						}
					}
				}
			}
		}

		private void toolSortByKind_Click(object sender, EventArgs e)
		{
			parent.SortMembers(SortingMode.ByKind);
			FillMembersList();
			Changed = true;
		}

		private void toolSortByAccess_Click(object sender, EventArgs e)
		{
			parent.SortMembers(SortingMode.ByAccess);
			FillMembersList();
			Changed = true;
		}

		private void toolSortByName_Click(object sender, EventArgs e)
		{
			parent.SortMembers(SortingMode.ByName);
			FillMembersList();
			Changed = true;
		}

		private void toolMoveUp_Click(object sender, EventArgs e)
		{
			if (lstMembers.SelectedItems.Count > 0) {
				ListViewItem item1 = lstMembers.SelectedItems[0];
				int index = item1.Index;

				if (index > 0) {
					ListViewItem item2 = lstMembers.Items[index - 1];

					if (item1.Tag is Field && item2.Tag is Field ||
						item1.Tag is Operation && item2.Tag is Operation)
					{
						locked = true;
						parent.MoveUpItem(item1.Tag);
						SwapListItems(item1, item2);
						item2.Focused = true;
						item2.Selected = true;
						locked = false;
						Changed = true;
					}
				}
			}
		}

		private void toolMoveDown_Click(object sender, EventArgs e)
		{
			if (lstMembers.SelectedItems.Count > 0) {
				ListViewItem item1 = lstMembers.SelectedItems[0];
				int index = item1.Index;

				if (index < lstMembers.Items.Count - 1) {
					ListViewItem item2 = lstMembers.Items[index + 1];

					if (item1.Tag is Field && item2.Tag is Field ||
						item1.Tag is Operation && item2.Tag is Operation)
					{
						locked = true;
						parent.MoveDownItem(item1.Tag);
						SwapListItems(item1, item2);
						item2.Focused = true;
						item2.Selected = true;
						locked = false;
						Changed = true;
					}
				}
			}
		}

		private void toolDelete_Click(object sender, EventArgs e)
		{
			DeleteSelectedMember();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}