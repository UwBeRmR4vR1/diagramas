﻿// NClass - UML class diagram editor
// Copyright (C) 2006 Balázs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using NClass.Core;

namespace NClass.GUI.Diagram
{
	public delegate void EntityRemovedEventHandler(object sender, EntityRemovedEventArgs e);

	public class EntityRemovedEventArgs : EventArgs
	{
		Entity entity;

		public Entity Entity
		{
			get { return entity; }
		}

		public EntityRemovedEventArgs(Entity entity)
		{
			this.entity = entity;
		}
	}
}
