// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using NClass.Translations;

namespace NClass.Core
{
	public sealed class Generalization : Relation
	{
		/// <exception cref="ArgumentException">
		/// Cannot create <see cref="Generalization"/> between 
		/// <paramref name="derivedClass"/> and <paramref name="baseClass"/>.
		/// </exception>
		/// <exception cref="ArgumentNullException">
		/// <paramref name="first"/> is null.-or-
		/// <paramref name="second"/> is null.
		/// </exception>
		internal Generalization(TypeBase derivedClass, TypeBase baseClass)
			: base(derivedClass, baseClass)
		{
			if (!(derivedClass is IInheritable) || !((IInheritable) derivedClass).IsAllowedChild)
				throw new ArgumentException("Cannot be child class.", "derivedClass");
			if (!(baseClass is IInheritable) || !((IInheritable) baseClass).IsAllowedParent)
				throw new ArgumentException("Cannot be base class.", "baseClass");
			if (derivedClass.GetType() != baseClass.GetType())
				throw new ArgumentException("Classes are not the same type.");
			if (derivedClass == baseClass)
				throw new ArgumentException("Cannot inherit from the same class.");
			if (derivedClass is ClassType && ((ClassType) derivedClass).HasBase)
				throw new ArgumentException("Cannot have multiple bases.");
			if (baseClass.Parent == derivedClass) //TODO: nem Parent, Ancestor()
				throw new ArgumentException("Nested class cannot be base class.");
			if (baseClass.Language == Language.CSharp && derivedClass.Parent == baseClass)
				throw new ArgumentException("Nested class cannot be child class.");
		}

		public override string ToString()
		{
			return string.Format("{0}: {1} --> {2}",
				Texts.GetText("generalization"), First, Second);
		}
	}
}
