// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using NClass.Translations;

namespace NClass.Core
{
	public sealed class CommentRelation : Relation
	{
		/// <exception cref="ArgumentNullException">
		/// <paramref name="comment"/> is null.-or-
		/// <paramref name="entity"/> is null.
		/// </exception>
		internal CommentRelation(Comment comment, Entity entity) : base(comment, entity)
		{
		}

		/// <exception cref="ArgumentNullException">
		/// <paramref name="entity"/> is null.-or-
		/// <paramref name="comment"/> is null.
		/// </exception>
		internal CommentRelation(Entity entity, Comment comment) : base(entity, comment)
		{
		}

		public override string ToString()
		{
			return string.Format("{0}: {1} --- {2}",
				Texts.GetText("comment"), First, Second);
		}
	}
}
