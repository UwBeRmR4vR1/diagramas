// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using NClass.Translations;

namespace NClass.Core
{
	public sealed class Nesting : Relation
	{
		/// <exception cref="ArgumentException">
		/// Cannot create <see cref="Nesting"/> between 
		/// <paramref name="parentClass"/> and <paramref name="nestedType"/>.
		/// </exception>
		/// <exception cref="ArgumentNullException">
		/// <paramref name="first"/> is null.-or-
		/// <paramref name="second"/> is null.
		/// </exception>
		internal Nesting(FieldContainer parentClass, TypeBase nestedType)
			: base(parentClass, nestedType)
		{
			if (parentClass == nestedType)
				throw new ArgumentException("The class cannot contain itself.");
			if (parentClass is ClassType && ((ClassType) parentClass).Base == nestedType)
				throw new ArgumentException("Nested class cannot be base class.");
			if (parentClass.Language == Language.CSharp && nestedType is ClassType &&
				((ClassType) nestedType).Base == parentClass)
			{
				throw new ArgumentException("Nested class cannot be child class.");
			}
			if (nestedType.IsNested) {
				throw new ArgumentException(
					"Nested types cannot have multiple parents.", "nestedType");
			}
		}

		public override string ToString()
		{
			return string.Format("{0}: {1} (+)--> {2}",
				Texts.GetText("nesting"), First, Second);
		}
	}
}
