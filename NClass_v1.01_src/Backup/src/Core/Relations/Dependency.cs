// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Xml;
using NClass.Translations;

namespace NClass.Core
{
	public sealed class Dependency : Relation
	{
		/// <exception cref="ArgumentNullException">
		/// <paramref name="first"/> is null.-or-
		/// <paramref name="second"/> is null.
		/// </exception>
		internal Dependency(TypeBase first, TypeBase second) : base(first, second)
		{
		}

		public override string ToString()
		{
			return string.Format("{0}: {1} --> {3}",
				Texts.GetText("dependency"), First, Second);
		}
	}
}
