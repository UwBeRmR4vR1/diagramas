// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Xml;

namespace NClass.Core
{
	public abstract class Relation
	{
		Entity first;
		Entity second;

		/// <exception cref="ArgumentNullException">
		/// <paramref name="first"/> is null.-or-
		/// <paramref name="second"/> is null.
		/// </exception>
		protected Relation(Entity first, Entity second)
		{
			if (first == null)
				throw new ArgumentNullException("first");
			if (second == null)
				throw new ArgumentNullException("second");

			this.first = first;
			this.second = second;
		}

		public Entity First
		{
			get { return first; }
		}

		public Entity Second
		{
			get { return second; }
		}

		/// <exception cref="ArgumentNullException">
		/// <paramref name="node"/> is null.
		/// </exception>
		internal virtual void Serialize(XmlNode node)
		{
			if (node == null)
				throw new ArgumentNullException("node");
		}

		/// <exception cref="ArgumentNullException">
		/// <paramref name="node"/> is null.
		/// </exception>
		internal virtual void Deserialize(XmlNode node)
		{
			if (node == null)
				throw new ArgumentNullException("node");
		}
	}
}
