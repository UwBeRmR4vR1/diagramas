﻿// NClass - UML class diagram editor
// Copyright (C) 2006 Balázs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Collections;

namespace NClass.Core
{
	public abstract class ParameterCollection : CollectionBase, ICloneable
	{
		protected ParameterCollection()
		{
		}

		/// <exception cref="ArgumentNullException">
		/// <paramref name="collection"/> is null.
		/// </exception>
		protected ParameterCollection(ParameterCollection collection)
		{
			if (collection == null)
				throw new ArgumentNullException("collection");

			Capacity = collection.Capacity;
			for (int i = 0; i < collection.InnerList.Count; i++)
				InnerList.Add(collection.InnerList[i]);
		}

		public Parameter this[int index]
		{
			get { return InnerList[index] as Parameter; }
		}

		public static ParameterCollection GetCollection(Language language)
		{
			if (language == Language.CSharp)
				return new CSharpParameterCollection();
			else
				return new JavaParameterCollection();
		}

		public void Remove(Parameter parameter)
		{
			if (!InnerList.IsReadOnly && !InnerList.IsFixedSize)
				InnerList.Remove(parameter);
		}

		protected bool ReservedName(string name)
		{
			return ReservedName(name, -1);
		}

		protected bool ReservedName(string name, int index)
		{
			for (int i = 0; i < Count; i++) {
				if (((Parameter) InnerList[i]).Name == name && i != index)
					return true;
			}
			return false;
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="declaration"/> does not fit to the syntax.
		/// </exception>
		/// <exception cref="ReservedNameException">
		/// The parameter name is already exists.
		/// </exception>
		public abstract Parameter Add(string declaration);

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="declaration"/> does not fit to the syntax.
		/// </exception>
		/// <exception cref="ReservedNameException">
		/// The parameter name is already exists.
		/// </exception>
		public abstract Parameter ModifyParameter(Parameter parmeter, string declaration);

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="declaration"/> does not fit to the syntax.
		/// </exception>
		public abstract void InitFromString(string declaration);

		public abstract object Clone();
	}
}
