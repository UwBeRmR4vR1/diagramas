// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Text.RegularExpressions;
using NClass.Translations;

namespace NClass.Core
{
	public abstract class Parameter
	{
		string type;
		string name;
		ParameterModifier modifier;

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> or <paramref name="type"/> 
		/// does not fit to the syntax.
		/// </exception>
		protected Parameter(string name, string type, ParameterModifier modifier)
		{
			Name = name;
			Type = type;
			Modifier = modifier;
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="value"/> does not fit to the syntax.
		/// </exception>
		public string Name
		{
			get
			{
				return name;
			}
			private set
			{
				if (SyntaxHelper.IsForbiddenName(value, Language)) {
					throw new BadSyntaxException(
						Texts.GetText("error_invalid_parameter_declaration"));
				}
				name = value;
			}
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="value"/> does not fit to the syntax.
		/// </exception>
		public string Type
		{
			get
			{
				return type;
			}
			private set
			{
				if (SyntaxHelper.IsForbiddenTypeName(value, Language) || value == "void") {
					throw new BadSyntaxException(
						Texts.GetText("error_invalid_parameter_declaration"));
				}
				type = value;
			}
		}

		public ParameterModifier Modifier
		{
			get
			{
				return modifier;
			}
			private set
			{
				modifier = value;
			}
		}

		public abstract Language Language
		{
			get;
		}

		public string GetCaption(bool getName)
		{
			string typeAndModifier;

			if (Modifier == ParameterModifier.None)
				typeAndModifier = Type;
			else
				typeAndModifier = Modifier.ToString().ToLower() + " " + Type;

			if (getName)
				return Name + ": " + typeAndModifier;
			else
				return typeAndModifier;
		}

		public override string ToString()
		{
			if (Modifier == ParameterModifier.None) {
				return Type + " " + Name;
			}
			else {
				return string.Format("{0} {1} {2}",
					Modifier.ToString().ToLower(), Type, Name);
			}

		}
	}
}
