// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;

namespace NClass.Core
{
	internal sealed class CSharpParameter : Parameter
	{
		const string ParameterPattern =
			@"\s*(?<modifier>out|ref|params)?(?(modifier)\s+|)" +
			@"(?<type>" + SyntaxHelper.GenericTypePattern2 + @")\s+" +
			@"(?<name>" + SyntaxHelper.NamePattern + @")\s*(,|$)";

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> or <paramref name="type"/> 
		/// does not fit to the syntax.
		/// </exception>
		internal CSharpParameter(string name, string type, ParameterModifier modifier)
			: base(name, type, modifier)
		{
		}

		public override Language Language
		{
			get { return Language.CSharp; }
		}
	}
}
