// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Xml;
using System.Text;
using System.Text.RegularExpressions;
using NClass.Translations;

namespace NClass.Core
{
	public abstract class Field : Member
	{
		bool isReadonly;
		string initialValue;

		const string NamePattern =
			@"^\s*" + "(?<name>" + SyntaxHelper.NamePattern + @")\s*$";

		static Regex nameRegex = new Regex(NamePattern, RegexOptions.ExplicitCapture);
		static Regex typeRegex = new Regex(TypePattern, RegexOptions.ExplicitCapture);

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> does not fit to the syntax.
		/// </exception>
		/// <exception cref="ArgumentNullException">
		/// <paramref name="parent"/> is null.
		/// </exception>
		protected Field(string name, OperationContainer parent) : base(name, parent)
		{
			IsReadonly = false;
			InitialValue = null;
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="value"/> does not fit to the syntax.
		/// </exception>
		public override string Name
		{
			get
			{
				return base.Name;
			}
			set
			{
				Match match = nameRegex.Match(value);

				if (match.Success)
					base.Name = match.Groups["name"].Value;
				else
					throw new BadSyntaxException(Texts.GetText("error_invalid_name"));
			}
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="value"/> does not fit to the syntax.
		/// </exception>
		public override string Type
		{
			get
			{
				return base.Type;
			}
			set
			{
				if (value == "void")
					throw new BadSyntaxException(Texts.GetText("error_type", "void"));

				Match match = typeRegex.Match(value);

				if (match.Success)
					base.Type = match.Groups["type"].Value;
				else
					throw new BadSyntaxException(Texts.GetText("error_invalid_type_name"));
			}
		}

		protected override string DefaultType
		{
			get
			{
				return "int";
			}
		}

		public virtual bool IsReadonly
		{
			get { return isReadonly; }
			set { isReadonly = value; }
		}


		public string InitialValue
		{
			get { return initialValue; }
			set { initialValue = value; }
		}

		public bool HasInitialValue
		{
			get
			{
				return !string.IsNullOrEmpty(InitialValue);
			}
		}

		public override string GetCaption(bool getType, bool getParameters,
			bool getParameterNames, bool getInitValue)
		{
			StringBuilder builder = new StringBuilder(50);

			builder.Append(Name);
			if (getType)
				builder.AppendFormat(": {0}", Type);
			if (getInitValue && HasInitialValue)
				builder.AppendFormat(" = {0}", InitialValue);

			return builder.ToString();
		}
	}
}
