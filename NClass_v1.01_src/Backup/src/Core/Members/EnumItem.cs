﻿// NClass - UML class diagram editor
// Copyright (C) 2006 Balázs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Text.RegularExpressions;
using NClass.Translations;

namespace NClass.Core
{
	public class EnumItem
	{
		// <name> [= value]
		const string EnumNamePattern = "(?<name>" + SyntaxHelper.NamePattern + ")";
		const string EnumItemPattern = @"^\s*" + EnumNamePattern +
			@"(\s*=\s*(?<value>\d+))?\s*$";
		const int EmptyValue = 0;

		static Regex enumNameRegex = new Regex(EnumNamePattern, RegexOptions.ExplicitCapture);
		static Regex enumItemRegex = new Regex(EnumItemPattern, RegexOptions.ExplicitCapture);

		string name;
		int initValue;

		private EnumItem()
		{
		}

		public string Name
		{
			get { return name; }
		}

		public int InitValue
		{
			get { return initValue; }
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="declaration"/> does not fit to the syntax.
		/// </exception>
		public void InitFromString(string declaration)
		{
			Match match = enumItemRegex.Match(declaration);

			if (match.Success) {
				Group nameGroup = match.Groups["name"];
				Group valueGroup = match.Groups["value"];

				name = nameGroup.Value;
				if (valueGroup.Success)
					int.TryParse(valueGroup.Value, out initValue);
				else
					initValue = EmptyValue;
			}
			else {
				throw new BadSyntaxException(Texts.GetText("error_invalid_declaration"));
			}
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="declaration"/> does not fit to the syntax.
		/// </exception>
		internal static EnumItem LoadFromString(string declaration)
		{
			EnumItem value = new EnumItem();

			value.InitFromString(declaration);
			return value;
		}

		public override string ToString()
		{
			if (InitValue == EmptyValue)
				return Name.ToString();
			else
				return Name + " = " + InitValue;
		}
	}
}