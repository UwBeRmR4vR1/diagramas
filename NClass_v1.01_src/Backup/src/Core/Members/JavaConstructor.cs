// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Text.RegularExpressions;
using NClass.Translations;

namespace NClass.Core
{
	internal sealed class JavaConstructor : Constructor
	{
		// [<access>] <name>([<args>])
		const string ConstructorPattern =
			@"^\s*" + JavaAccessPattern +
			@"(?<name>" + SyntaxHelper.NamePattern + ")" +
			@"\((?(static)|(?<args>.*))\)" + SyntaxHelper.DeclarationEnding;

		static Regex constructorRegex =
			new Regex(ConstructorPattern, RegexOptions.ExplicitCapture);

		/// <exception cref="ArgumentNullException">
		/// <paramref name="parent"/> is null.
		/// </exception>
		internal JavaConstructor(OperationContainer parent) : base(parent)
		{
		}		

		public override AccessModifier DefaultAccess
		{
			get
			{
				return Parent.Access;
			}
		}

		/// <exception cref="BadSyntaxException">
		/// Cannot set static modifier.
		/// </exception>
		public override bool IsStatic
		{
			get
			{
				return base.IsStatic;
			}
			set
			{
				if (value)
					throw new BadSyntaxException(Texts.GetText("error_cannot_set_static"));
			}
		}

		public override Language Language
		{
			get { return Language.Java; }
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="declaration"/> does not fit to the syntax.
		/// </exception>
		public override void InitFromString(string declaration)
		{
			Match match = constructorRegex.Match(declaration);

			if (match.Success) {
				try {
					Group nameGroup   = match.Groups["name"];
					Group accessGroup = match.Groups["access"];
					Group argsGroup   = match.Groups["args"];

					Name = nameGroup.Value;
					AccessModifier = SyntaxHelper.ParseAccessModifier(accessGroup.Value);
					ParameterList.InitFromString(argsGroup.Value);
				}
				catch (BadSyntaxException ex) {
					throw new BadSyntaxException(Texts.GetText("error_invalid_declaration"), ex);
				}
			}
			else {
				throw new BadSyntaxException(Texts.GetText("error_invalid_declaration"));
			}
		}
	}
}
