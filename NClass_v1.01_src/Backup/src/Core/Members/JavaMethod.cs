// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Text;
using System.Text.RegularExpressions;
using NClass.Translations;

namespace NClass.Core
{
	internal sealed class JavaMethod : Method
	{
		// [<access>] [<modifier> | static] <type> <name>(<args>)
		const string MethodPattern =
			@"^\s*" + JavaAccessPattern + JavaModifierPattern + StaticPattern +
			@"(?<type>" + SyntaxHelper.GenericTypePattern2 + @")\s+" +
			@"(?<name>" + SyntaxHelper.NamePattern + ")" +
			@"\((?<args>.*)\)" + SyntaxHelper.DeclarationEnding;

		const string NamePattern =
			@"^\s*(?<name>" + SyntaxHelper.NamePattern + @")\s*$";

		static Regex methodRegex = new Regex(MethodPattern, RegexOptions.ExplicitCapture);
		static Regex nameRegex = new Regex(NamePattern, RegexOptions.ExplicitCapture);
		static Regex typeRegex = new Regex(TypePattern, RegexOptions.ExplicitCapture);

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> does not fit to the syntax.
		/// </exception>
		/// <exception cref="ArgumentNullException">
		/// <paramref name="parent"/> is null.
		/// </exception>
		internal JavaMethod(string name, OperationContainer parent) : base(name, parent)
		{
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="value"/> does not fit to the syntax.
		/// </exception>
		public override string Name
		{
			get
			{
				return base.Name;
			}
			set
			{
				Match match = nameRegex.Match(value);

				if (match.Success)
					base.Name = match.Groups["name"].Value;
				else
					throw new BadSyntaxException(Texts.GetText("error_invalid_name"));
			}
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="value"/> does not fit to the syntax.
		/// </exception>
		public override string Type
		{
			get
			{
				return base.Type;
			}
			set
			{
				Match match = typeRegex.Match(value);

				if (match.Success)
					base.Type = match.Groups["type"].Value;
				else
					throw new BadSyntaxException(Texts.GetText("error_invalid_type_name"));
			}
		}

		/// <exception cref="BadSyntaxException">
		/// Cannot set modifier.
		/// </exception>
		public override OperationModifier Modifier
		{
			get
			{
				return base.Modifier;
			}
			set
			{
				if (value == OperationModifier.Override || value == OperationModifier.Virtual)
					base.Modifier = OperationModifier.None;
				else
					base.Modifier = value;
			}
		}

		public override Language Language
		{
			get { return Language.Java; }
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="declaration"/> does not fit to the syntax.
		/// </exception>
		public override void InitFromString(string declaration)
		{
			Match match = methodRegex.Match(declaration);

			if (match.Success) {
				try {
					Group nameGroup     = match.Groups["name"];
					Group typeGroup     = match.Groups["type"];
					Group accessGroup   = match.Groups["access"];
					Group modifierGroup = match.Groups["modifier"];
					Group staticGroup   = match.Groups["static"];
					Group argsGroup     = match.Groups["args"];

					Name = nameGroup.Value;
					Type = typeGroup.Value;
					AccessModifier = SyntaxHelper.ParseAccessModifier(accessGroup.Value);
					Modifier = SyntaxHelper.ParseOperationModifier(modifierGroup.Value);
					IsStatic = staticGroup.Success;
					ParameterList.InitFromString(argsGroup.Value);
				}
				catch (BadSyntaxException ex) {
					throw new BadSyntaxException(Texts.GetText("error_invalid_declaration"), ex);
				}
			}
			else {
				throw new BadSyntaxException(Texts.GetText("error_invalid_declaration"));
			}
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder(100);

			if (AccessModifier != AccessModifier.Default) {
				builder.Append(SyntaxHelper.GetAccessModifier(AccessModifier, Language.Java));
				builder.Append(" ");
			}
			if (Modifier != OperationModifier.None) {
				builder.Append(SyntaxHelper.GetOperationModifier(Modifier, Language.Java));
				builder.Append(" ");
			}
			if (IsStatic) {
				builder.Append("static ");
			}

			builder.AppendFormat("{0} {1}(", Type, Name);

			for (int i = 0; i < ParameterList.Count; i++) {
				builder.Append(ParameterList[i]);
				if (i < ParameterList.Count - 1)
					builder.Append(", ");
			}
			builder.Append(")");

			return builder.ToString();
		}
	}
}
