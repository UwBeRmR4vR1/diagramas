// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;

namespace NClass.Core
{
	public enum OperationModifier
	{
		None,

		/// <summary>
		/// Abstract members must be implemented by classes
		/// that derive from the abstract class.
		/// </summary>
		Abstract,

		/// <summary>
		/// Provides a new implementation of a virtual member
		/// inherited from a base class.
		/// </summary>
		Override,

		/// <summary>
		/// Declares a method whose implementation can be changed by an 
		/// overriding member in a derived class.
		/// </summary>
		Virtual,

		/// <summary>
		/// A sealed method overrides a method in a base class,
		/// but itself cannot be overridden further in any derived class.
		/// </summary>
		Sealed
	}
}
