﻿// NClass - UML class diagram editor
// Copyright (C) 2006 Balázs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Runtime.Serialization;
using NClass.Translations;

namespace NClass.Core
{
	public class ReservedNameException : BadSyntaxException
	{
		string name;

		public ReservedNameException() : base(Texts.GetText("error_reserved_name"))
		{
			name = null;
		}

		public ReservedNameException(string name)
			: base(Texts.GetText("error_reserved_name"))
		{
			this.name = name;
		}

		public ReservedNameException(string name, Exception innerException)
			: base(Texts.GetText("error_reserved_name"), innerException)
		{
			this.name = name;
		}

		public string ReservedName
		{
			get { return name; }
		}
	}
}
