// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Collections.Generic;

namespace NClass.Core
{
	public interface IInterfaceImplementer : IOperationAllower
	{
		IEnumerable<InterfaceType> Interfaces
		{
			get;
		}

		bool ImplementsInterface
		{
			get;
		}

		/// <exception cref="ArgumentException">
		/// The language of <paramref name="operation"/> does not equal.
		/// </exception>
		/// <exception cref="ArgumentNullException">
		/// <paramref name="operation"/> is null.
		/// </exception>
		Operation Implement(Operation operation);

		/// <exception cref="ArgumentException">
		/// The language of <paramref name="interfaceType"/> does not equal.-or-
		/// <paramref name="interfaceType"/> is earlier implemented interface.
		/// </exception>
		/// <exception cref="ArgumentNullException">
		/// <paramref name="interfaceType"/> is null.
		/// </exception>
		void AddInterface(InterfaceType interfaceType);

		void RemoveInterface(InterfaceType interfaceType);
	}
}
