﻿// NClass - UML class diagram editor
// Copyright (C) 2006 Balázs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Collections.Generic;

namespace NClass.Core
{
	public interface IOverridableType : IInheritable
	{
		InheritanceModifier Modifier
		{
			get;
			set;
		}

		/// <exception cref="ArgumentException">
		/// The language of <paramref name="value"/> does not equal.-or-
		/// The <paramref name="value"/> is descendant of the type.
		/// </exception>
		IOverridableType Base
		{
			get;
			set;
		}

		IEnumerable<Operation> OverridableOperations
		{
			get;
		}

		/// <exception cref="ArgumentException">
		/// <paramref name="operation"/> cannot be overridden.
		/// </exception>
		/// <exception cref="ArgumentNullException">
		/// <paramref name="operation"/> is null.
		/// </exception>
		Operation Override(Operation operation);
	}
}
