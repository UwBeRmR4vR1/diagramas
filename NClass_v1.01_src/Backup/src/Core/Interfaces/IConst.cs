// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

namespace NClass.Core
{
	public interface IConst
	{
		/// <exception cref="BadSyntaxException">
		/// <see cref="IsConst"/> is set to true while 
		/// <see cref="Field.InitialValue"/> is empty.
		/// </exception>
		bool IsConst
		{
			get;
			set;
		}

		bool CanSetConst
		{
			get;
		}
	}
}
