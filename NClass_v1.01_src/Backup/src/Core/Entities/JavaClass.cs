// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Collections.Generic;
using NClass.Translations;

namespace NClass.Core
{
	internal sealed class JavaClass : ClassType
	{
		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> does not fit to the syntax.
		/// </exception>
		internal JavaClass(string name) : base(name)
		{
		}

		public override AccessModifier DefaultMemberAccess
		{
			get { return AccessModifier.Internal; }
		}

		public override bool CanAddConstructor
		{
			get
			{
				return Modifier != InheritanceModifier.Static;
			}
		}

		public override string Signature
		{
			get
			{
				return SyntaxHelper.GetAccessModifier(
					Access, Language.Java, false) + " Class";
			}
		}

		public override Language Language
		{
			get { return Language.Java; }
		}

		/// <exception cref="ArgumentException">
		/// The language of <paramref name="interfaceType"/> does not equal.-or-
		/// <paramref name="interfaceType"/> is earlier implemented interface.
		/// </exception>
		/// <exception cref="ArgumentNullException">
		/// <paramref name="interfaceType"/> is null.
		/// </exception>
		internal override void AddInterface(InterfaceType interfaceType)
		{
			if (!(interfaceType is JavaInterface)) {
				throw new ArgumentException(Texts.GetText(
					"error_interface_language", "Java"), "interfaceType");
			}

			base.AddInterface(interfaceType);
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> does not fit to the syntax.
		/// </exception>
		public override Field AddField(string name)
		{
			Field field = new JavaField(name, this);

			FieldList.Add(field);
			return field;
		}

		public override Method AddConstructor()
		{
			Constructor constructor = new JavaConstructor(this);

			if (Modifier == InheritanceModifier.Abstract)
				constructor.AccessModifier = AccessModifier.Protected;
			else
				constructor.AccessModifier = AccessModifier.Public;
			OperationList.Add(constructor);
			return constructor;
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> does not fit to the syntax.
		/// </exception>
		public override Method AddMethod(string name)
		{
			Method method = new JavaMethod(name, this);

			method.AccessModifier = AccessModifier.Public;
			OperationList.Add(method);
			return method;
		}

		/// <exception cref="ArgumentException">
		/// <paramref name="operation"/> cannot be overridden.
		/// </exception>
		/// <exception cref="ArgumentNullException">
		/// <paramref name="operation"/> is null.
		/// </exception>
		public override Operation Override(Operation operation)
		{
			if (operation == null)
				throw new ArgumentNullException("operation");

			if (operation.Modifier == OperationModifier.Sealed) {
				throw new ArgumentException(
					Texts.GetText("error_cannot_override"), "operation");
			}
			if (operation.Language != Language.Java)
				throw new ArgumentException(Texts.GetText("error_languages_do_not_equal"));

			Operation newOperation = (Operation) operation.Clone();

			newOperation.Parent = this;
			newOperation.Modifier = OperationModifier.None;
			OperationList.Add(newOperation);

			return newOperation;
		}
	}
}
