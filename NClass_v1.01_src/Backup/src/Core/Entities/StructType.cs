// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using NClass.Translations;

namespace NClass.Core
{
	public sealed class StructType : FieldContainer
	{
		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> does not fit to the syntax.
		/// </exception>
		internal StructType(string name) : base(name)
		{
		}

		public override string Signature
		{
			get
			{
				return SyntaxHelper.GetAccessModifier(
					Access, Language.CSharp, false) + " Struct";
			}
		}

		public override string Stereotype
		{
			get { return "�struct�"; }
		}

		public override Language Language
		{
			get { return Language.CSharp; }
		}

		public override Method AddConstructor()
		{
			Constructor constructor = new CSharpConstructor(this);
			
			constructor.AccessModifier = AccessModifier.Public;
			OperationList.Add(constructor);
			return constructor;
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> does not fit to the syntax.
		/// </exception>
		public override Method AddMethod(string name)
		{
			Method method = new CSharpMethod(name, this);

			method.AccessModifier = AccessModifier.Public;
			OperationList.Add(method);
			return method;
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> does not fit to the syntax.
		/// </exception>
		public override Field AddField(string name)
		{
			Field field = new CSharpField(name, this);

			field.AccessModifier = AccessModifier.Public;
			FieldList.Add(field);
			return field;
		}

		/// <exception cref="ArgumentException">
		/// The language of <paramref name="interfaceType"/> does not equal.-or-
		/// <paramref name="interfaceType"/> is earlier implemented interface.
		/// </exception>
		/// <exception cref="ArgumentNullException">
		/// <paramref name="interfaceType"/> is null.
		/// </exception>
		internal override void AddInterface(InterfaceType interfaceType)
		{
			if (!(interfaceType is CSharpInterface)) {
				throw new ArgumentException(Texts.GetText(
					"error_interface_language", "C#"), "interfaceType");
			}

			base.AddInterface(interfaceType);
		}
	}
}
