// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Collections.Generic;
using System.Xml;

namespace NClass.Core
{
	public abstract class OperationContainer : TypeBase, IMethodAllower
	{
		List<Operation> operationList;

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> does not fit to the syntax.
		/// </exception>
		protected OperationContainer(string name) : base(name)
		{
			operationList = new List<Operation>();
		}

		public IEnumerable<Operation> Operations
		{
			get
			{
				for (int i = 0; i < OperationList.Count; i++)
					yield return OperationList[i];
			}
		}

		protected List<Operation> OperationList
		{
			get { return operationList; }
		}

		public int OperationCount
		{
			get { return OperationList.Count; }
		}

		public virtual AccessModifier DefaultMemberAccess
		{
			get { return AccessModifier.Private; }
		}

		public virtual void RemoveMember(Member member)
		{
			if (member is Operation)
				OperationList.Remove((Operation) member);
		}

		public override void MoveUpItem(object item)
		{
			if (item is Operation)
				TypeHelper.MoveUpItem(OperationList, (Operation) item);
		}

		public override void MoveDownItem(object item)
		{
			if (item is Operation)
				TypeHelper.MoveDownItem(OperationList, (Operation) item);
		}

		public virtual void SortMembers(SortingMode sortingMode)
		{
			switch (sortingMode) {
				case SortingMode.ByName:
					OperationList.Sort(TypeHelper.MemberComparisonByName);
					break;
				case SortingMode.ByAccess:
					OperationList.Sort(TypeHelper.MemberComparisonByAccess);
					break;
				case SortingMode.ByKind:
					OperationList.Sort(TypeHelper.MemberComparisonByKind);
					break;
				default:
					break;
			}
		}

		/// <exception cref="ArgumentNullException">
		/// <paramref name="node"/> is null.
		/// </exception>
		internal override void Serialize(XmlNode node)
		{
			base.Serialize(node);
			TypeHelper.SaveMembers(OperationList, "Operation", node);
		}

		/// <exception cref="BadSyntaxException">
		/// An error occured in building the class.
		/// </exception>
		/// <exception cref="ArgumentNullException">
		/// <paramref name="node"/> is null.
		/// </exception>
		internal override void Deserialize(XmlNode node)
		{
			base.Deserialize(node);
			TypeHelper.LoadMembers(OperationList, "Operation", node, this);
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> does not fit to the syntax.
		/// </exception>
		public abstract Method AddMethod(string name);
	}
}
