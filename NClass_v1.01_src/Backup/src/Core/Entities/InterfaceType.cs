﻿// NClass - UML class diagram editor
// Copyright (C) 2006 Balázs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Collections.Generic;
using NClass.Translations;

namespace NClass.Core
{
	public abstract class InterfaceType : OperationContainer, IInheritable
	{
		List<InterfaceType> baseList;

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> does not fit to the syntax.
		/// </exception>
		internal InterfaceType(string name) : base(name)
		{
			baseList = new List<InterfaceType>();
		}

		public IEnumerable<InterfaceType> Bases
		{
			get
			{
				for (int i = 0; i < BaseList.Count; i++)
					yield return BaseList[i];
			}
		}

		public bool HasBase
		{
			get { return BaseList.Count > 0; }
		}

		protected List<InterfaceType> BaseList
		{
			get { return baseList; }
		}

		public bool IsAllowedParent
		{
			get { return true; }
		}

		public bool IsAllowedChild
		{
			get { return true; }
		}

		public override string Stereotype
		{
			get { return "«interface»"; }
		}

		private bool IsAncestor(InterfaceType _interface)
		{
			for (int i = 0; i < baseList.Count; i++) {
				if (baseList[i].IsAncestor(_interface))
					return true;
			}
			return (_interface == this);
		}

		/// <exception cref="ArgumentException">
		/// The language of <paramref name="_base"/> does not equal.-or-
		/// <paramref name="_base"/> is earlier added base.-or-
		/// <paramref name="_base"/> is descendant of the interface.
		/// </exception>
		/// <exception cref="ArgumentNullException">
		/// <paramref name="_base"/> is null.
		/// </exception>
		internal void AddBase(InterfaceType _base)
		{
			if (_base == null)
				throw new ArgumentNullException("_base");

			if (BaseList.Contains(_base)) {
				throw new ArgumentException(
					Texts.GetText("error_cannot_add_same_base_interface"));
			}
			if (_base.IsAncestor(this)) {
					throw new ArgumentException(Texts.GetText("error_cyclic_base",
						Texts.GetText("interface")), "value");
			}

			if (_base.Language != this.Language)
				throw new ArgumentException(Texts.GetText("error_languages_do_not_equal"));

			BaseList.Add(_base);
		}

		internal void RemoveBase(InterfaceType _base)
		{
			bool v = BaseList.Remove(_base);
		}
	}
}
