// NClass - UML class diagram editor
// Copyright (C) 2006 Bal�zs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Collections.Generic;

namespace NClass.Core
{
	internal sealed class JavaInterface : InterfaceType, IFieldAllower
	{
		List<JavaField> fieldList;

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> does not fit to the syntax.
		/// </exception>
		public JavaInterface(string name) : base(name)
		{
			fieldList = new List<JavaField>();
		}

		public IEnumerable<Field> Fields
		{
			get
			{
				for (int i = 0; i < FieldList.Count; i++)
					yield return FieldList[i];
			}
		}

		private List<JavaField> FieldList
		{
			get { return fieldList; }
		}

		public int FieldCount
		{
			get { return FieldList.Count; }
		}

		public override AccessModifier DefaultMemberAccess
		{
			get { return Access; }
		}

		public override string Signature
		{
			get
			{
				return SyntaxHelper.GetAccessModifier(
					Access, Language.Java, false) + " Interface";
			}
		}

		public override Language Language
		{
			get { return Language.Java; }
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> does not fit to the syntax.
		/// </exception>
		public Field AddField(string name)
		{
			JavaField field = new JavaField(name, this);

			field.AccessModifier = AccessModifier.Default;
			field.IsStatic = true;
			field.IsReadonly = true;
			FieldList.Add(field);
			return field;
		}

		/// <exception cref="BadSyntaxException">
		/// The <paramref name="name"/> does not fit to the syntax.
		/// </exception>
		public override Method AddMethod(string name)
		{
			Method method = new JavaMethod(name, this);

			method.Modifier = OperationModifier.None;
			method.AccessModifier = AccessModifier.Default;
			OperationList.Add(method);
			return method;
		}
	}
}
