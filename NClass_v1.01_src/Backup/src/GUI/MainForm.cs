﻿// NClass - UML class diagram editor
// Copyright (C) 2006 Balázs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.ComponentModel;
using System.Windows.Forms;
using NClass.Core;
using NClass.GUI.Diagram;
using NClass.Translations;

using WindowSettings = NClass.GUI.Properties.Settings;

namespace NClass.GUI
{
	public sealed partial class MainForm : Form
	{
		Project project;
		OptionsDialog optionsDialog;

		#region Enum orders

		static AccessModifier[] csharpAccessOrder = {
			AccessModifier.Public,
			AccessModifier.ProtectedInternal,
			AccessModifier.Internal,
			AccessModifier.Protected,
			AccessModifier.Private,
			AccessModifier.Default
		};

		static AccessModifier[] javaAccessOrder = {
			AccessModifier.Public,
			AccessModifier.Protected,
			AccessModifier.Private,
			AccessModifier.Default
		};

		static InheritanceModifier[] csharpModifierOrder = {
			InheritanceModifier.None,
			InheritanceModifier.Abstract,
			InheritanceModifier.Sealed,
			InheritanceModifier.Static
		};

		static InheritanceModifier[] javaModifierOrder = {
			InheritanceModifier.None,
			InheritanceModifier.Abstract,
			InheritanceModifier.Sealed,
			InheritanceModifier.Static
		};

		#endregion

		public MainForm()
		{
			Init();

			if (Settings.LoadLastProject && !string.IsNullOrEmpty(Settings.LastProject))
				LoadProject(Settings.LastProject);
			else
				project.NewProject();

			diagram.Select();
		}

		public MainForm(string fileName)
		{
			Init();
			LoadProject(fileName);
		}

		private void Init()
		{
			InitializeComponent();

			project = new Project(diagram);
			optionsDialog = new OptionsDialog();
			lblStatus.Text = Texts.GetText("ready");
			printDocument.DefaultPageSettings.Margins = new Margins(40, 40, 40, 40);
			project.StateChanged += new EventHandler(project_StateChanged);
			optionsDialog.Apply += new EventHandler(optionsDialog_Apply);
			Texts.LanguageChanged += new EventHandler(Texts_LanguageChanged);

			UpdateTexts();
		}

		private void UpdateTexts()
		{
			// File menu
			mnuFile.Text = Texts.GetText("menu_file");
			mnuNew.Text = Texts.GetText("menu_add_new");
			mnuNewCSharpDiagram.Text = Texts.GetText("menu_csharp_diagram");
			mnuNewJavaDiagram.Text = Texts.GetText("menu_java_diagram");
			mnuOpen.Text = Texts.GetText("menu_open");
			mnuOpenFile.Text = Texts.GetText("menu_new_file");
			mnuSave.Text = Texts.GetText("menu_save");
			mnuSaveAs.Text = Texts.GetText("menu_save_as");
			mnuPrintPreview.Text = Texts.GetText("menu_print_preview");
			mnuPrint.Text = Texts.GetText("menu_print");
			mnuExit.Text = Texts.GetText("menu_exit");

			// Diagram menu
			mnuDiagram.Text = Texts.GetText("menu_diagram");
			mnuAddNewElement.Text = Texts.GetText("menu_new");
			mnuNewClass.Text = Texts.GetText("menu_class");
			mnuNewStruct.Text = Texts.GetText("menu_struct");
			mnuNewInterface.Text = Texts.GetText("menu_interface");
			mnuNewEnum.Text = Texts.GetText("menu_enum");
			mnuNewDelegate.Text = Texts.GetText("menu_delegate");
			mnuNewComment.Text = Texts.GetText("menu_comment");
			mnuNewAssociation.Text = Texts.GetText("menu_association");
			mnuNewComposition.Text = Texts.GetText("menu_composition");
			mnuNewAggregation.Text = Texts.GetText("menu_aggregation");
			mnuNewGeneralization.Text = Texts.GetText("menu_generalization");
			mnuNewRealization.Text = Texts.GetText("menu_realization");
			mnuNewDependency.Text = Texts.GetText("menu_dependency");
			mnuNewNesting.Text = Texts.GetText("menu_nesting");
			mnuNewCommentRelation.Text = Texts.GetText("menu_comment_relation");
			mnuMembersFormat.Text = Texts.GetText("menu_members_format");
			mnuShowType.Text = Texts.GetText("menu_type");
			mnuShowParameters.Text = Texts.GetText("menu_parameters");
			mnuShowParameterNames.Text = Texts.GetText("menu_parameter_names");
			mnuShowInitialValue.Text = Texts.GetText("menu_initial_value");			
			mnuDelete.Text = Texts.GetText("menu_delete");
			mnuSaveAsImage.Text = Texts.GetText("menu_save_as_image");
			mnuAutoZoom.Text = Texts.GetText("menu_auto_zoom");
			mnuOptions.Text = Texts.GetText("menu_options");

			// Help menu
			mnuHelp.Text = Texts.GetText("menu_help");
			mnuContents.Text = Texts.GetText("menu_contents");
			mnuAbout.Text = Texts.GetText("menu_about");

			// Context menu
			mnuAddNewElementContext.Text = Texts.GetText("menu_new");
			mnuNewClassContext.Text = Texts.GetText("menu_class");
			mnuNewStructContext.Text = Texts.GetText("menu_struct");
			mnuNewInterfaceContext.Text = Texts.GetText("menu_interface");
			mnuNewEnumContext.Text = Texts.GetText("menu_enum");
			mnuNewDelegateContext.Text = Texts.GetText("menu_delegate");
			mnuNewCommentContext.Text = Texts.GetText("menu_comment");
			mnuNewAssociationContext.Text = Texts.GetText("menu_association");
			mnuNewCompositionContext.Text = Texts.GetText("menu_composition");
			mnuNewAggregationContext.Text = Texts.GetText("menu_aggregation");
			mnuNewGeneralizationContext.Text = Texts.GetText("menu_generalization");
			mnuNewRealizationContext.Text = Texts.GetText("menu_realization");
			mnuNewDependencyContext.Text = Texts.GetText("menu_dependency");
			mnuNewNestingContext.Text = Texts.GetText("menu_nesting");
			mnuNewCommentRelationContext.Text = Texts.GetText("menu_comment_relation");
			mnuMembersFormatContext.Text = Texts.GetText("menu_members_format");
			mnuShowTypeContext.Text = Texts.GetText("menu_type");
			mnuShowParametersContext.Text = Texts.GetText("menu_parameters");
			mnuShowParameterNamesContext.Text = Texts.GetText("menu_parameter_names");
			mnuShowInitialValueContext.Text = Texts.GetText("menu_initial_value");
			mnuSelectAllContext.Text = Texts.GetText("menu_select_all");
			mnuSaveAsImageContext.Text = Texts.GetText("menu_save_as_image");

			// Toolbar
			toolNewCSharpDiagram.Text = Texts.GetText("menu_csharp_diagram");
			toolNewJavaDiagram.Text = Texts.GetText("menu_java_diagram");
			toolSave.Text = Texts.GetText("save");
			toolPrint.Text = Texts.GetText("print");
			toolZoomIn.Text = Texts.GetText("zoom_in");
			toolZoomOut.Text = Texts.GetText("zoom_out");
			cboZoom.ToolTipText = Texts.GetText("zoom");
			toolAutoZoom.Text = Texts.GetText("auto_zoom");
			toolNewClass.Text = Texts.GetText("add_new_class");
			toolNewStruct.Text = Texts.GetText("add_new_struct");
			toolNewInterface.Text = Texts.GetText("add_new_interface");
			toolNewEnum.Text = Texts.GetText("add_new_enum");
			toolNewDelegate.Text = Texts.GetText("add_new_delegate");
			toolNewComment.Text = Texts.GetText("add_new_comment");
			toolNewAssociation.Text = Texts.GetText("add_new_association");
			toolNewComposition.Text = Texts.GetText("add_new_composition");
			toolNewAggregation.Text = Texts.GetText("add_new_aggregation");
			toolNewGeneralization.Text = Texts.GetText("add_new_generalization");
			toolNewRealization.Text = Texts.GetText("add_new_realization");
			toolNewDependency.Text = Texts.GetText("add_new_dependency");
			toolNewNesting.Text = Texts.GetText("add_new_nesting");
			toolNewCommentRelation.Text = Texts.GetText("add_new_comment_relation");
			toolDelete.Text = Texts.GetText("delete_selected_items");
			lblName.Text = Texts.GetText("name:");
			lblAccess.Text = Texts.GetText("access:");
			lblModifier.Text = Texts.GetText("modifier:");

			// Others
			saveAsImageDialog.Title = Texts.GetText("save_as_image");
		}

		private void LoadProject(string fileName)
		{
			try {
				this.Cursor = Cursors.WaitCursor;
				this.SuspendLayout();
				project.Load(fileName);
			}
			catch (Exception ex) {
				MessageBox.Show(Texts.GetText("error") + ": " + ex.Message,
					Texts.GetText("load"), MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			finally {
				this.ResumeLayout();
				this.Cursor = Cursors.Default;
			}
		}

		private Control PanelFromPosition(DockStyle dockStyle)
		{
			switch (dockStyle) {
				case DockStyle.Left:
					return toolStripContainer.LeftToolStripPanel;

				case DockStyle.Right:
					return toolStripContainer.RightToolStripPanel;

				case DockStyle.Bottom:
					return toolStripContainer.BottomToolStripPanel;

				case DockStyle.Top:
				default:
					return toolStripContainer.TopToolStripPanel;
			}
		}

		private DockStyle PositionFromPanel(Control control)
		{
			if (control == toolStripContainer.LeftToolStripPanel)
				return DockStyle.Left;

			if (control == toolStripContainer.RightToolStripPanel)
				return DockStyle.Right;

			if (control == toolStripContainer.BottomToolStripPanel)
				return DockStyle.Bottom;

			return DockStyle.Top;
		}

		private void LoadWindowSettings()
		{
			Location = WindowSettings.Default.WindowPosition;
			Size = WindowSettings.Default.WindowSize;
			if (WindowSettings.Default.IsWindowMaximized)
				WindowState = FormWindowState.Maximized;

			standardToolStrip.Parent =
				PanelFromPosition(WindowSettings.Default.StandardToolBarPosition);
			elementsToolStrip.Parent =
				PanelFromPosition(WindowSettings.Default.ElementsToolBarPosition);
			typeDetailsToolStrip.Parent =
				PanelFromPosition(WindowSettings.Default.TypeDetailsToolBarPosition);

			standardToolStrip.Location = WindowSettings.Default.StandardToolBarLocation;
			elementsToolStrip.Location = WindowSettings.Default.ElementsToolBarLocation;
			typeDetailsToolStrip.Location = WindowSettings.Default.TypeDetailsToolBarLocation;
		}

		private void SaveWindowSettings()
		{
			if (WindowState == FormWindowState.Maximized) {
				WindowSettings.Default.IsWindowMaximized = true;
			}
			else {
				WindowSettings.Default.IsWindowMaximized = false;
				if (WindowState == FormWindowState.Normal)
					WindowSettings.Default.WindowSize = Size;
				if (WindowState == FormWindowState.Normal)
					WindowSettings.Default.WindowPosition = Location;
			}

			WindowSettings.Default.StandardToolBarPosition = PositionFromPanel(
				standardToolStrip.Parent);
			WindowSettings.Default.ElementsToolBarPosition = PositionFromPanel(
				elementsToolStrip.Parent);
			WindowSettings.Default.TypeDetailsToolBarPosition = PositionFromPanel(
				typeDetailsToolStrip.Parent);

			WindowSettings.Default.StandardToolBarLocation = standardToolStrip.Location;
			WindowSettings.Default.ElementsToolBarLocation = elementsToolStrip.Location;
			WindowSettings.Default.TypeDetailsToolBarLocation = typeDetailsToolStrip.Location;

			WindowSettings.Default.Save();
		}

		private void SaveAsImage()
		{
			if (diagram.ItemCount == 0)
				return;

			saveAsImageDialog.FileName = project.ProjectFileNameWithoutExtension;
			saveAsImageDialog.InitialDirectory = project.ProjectDirectory;

			if (saveAsImageDialog.ShowDialog() == DialogResult.OK) {
				string extension = Path.GetExtension(saveAsImageDialog.FileName);
				ImageFormat format;

				switch (extension.ToLower()) {
					case ".bmp":
						format = ImageFormat.Bmp; break;
					case ".gif":
						format = ImageFormat.Gif; break;
					case ".jpg":
					case ".jpeg":
						format = ImageFormat.Jpeg; break;
					case ".png":
					default:
						format = ImageFormat.Png; break;
				}
				diagram.SaveAsImage(saveAsImageDialog.FileName, format);
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			LoadWindowSettings();
		}

		protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
		{
			base.OnClosing(e);

			if (!project.CanClose())
				e.Cancel = true;
			SaveWindowSettings();
		}

		protected override void OnDragEnter(DragEventArgs e)
		{
			base.OnDragEnter(e);

			if (e.Data.GetDataPresent(DataFormats.FileDrop))
				e.Effect = DragDropEffects.Copy;
			else
				e.Effect = DragDropEffects.None;
		}

		protected override void OnDragDrop(DragEventArgs e)
		{
			base.OnDragDrop(e);

			if (e.Data.GetDataPresent(DataFormats.FileDrop)) {
				string[] files = (string[]) e.Data.GetData(DataFormats.FileDrop);
				if (files.Length > 0)
					LoadProject(files[0]);
			}
		}

		private void UpdateStatusBar()
		{
			int selectedItemCount = diagram.SelectedItemCount;

			if (selectedItemCount == 0)
				lblStatus.Text = Texts.GetText("ready");
			else if (selectedItemCount == 1)
				lblStatus.Text = diagram.FirstSelectedItem.ToString();
			else
				lblStatus.Text = Texts.GetText("items_selected", selectedItemCount);

			if (project.Language == Language.CSharp)
				lblLanguage.Text = Texts.GetText("language") + ": C#";
			else
				lblLanguage.Text = Texts.GetText("language") + ": Java";
		}

		private void UpdateHeader()
		{
			this.Text = "NClass - " + project.ToString();
		}

		private void UpdateWindow()
		{
			UpdateHeader();
			diagram.AutoScrollMinSize = new Size(
				Settings.WorkspaceWidth, Settings.WorkspaceHeight);
			diagram.Refresh();
		}

		private void UpdateModifiersToolBar()
		{
			int selectedItemCount = diagram.SelectedItemCount;
			IDiagramElement selectedItem = diagram.FirstSelectedItem;

			if (selectedItemCount == 1 && selectedItem is TypeShape) {
				TypeBase type = (TypeBase) ((TypeShape) selectedItem).Entity;

				txtName.Text = type.Name;
				txtName.Enabled = true;
				SetAccessLabel(type.AccessModifier);
				cboAccess.Enabled = true;

				if (type is ClassType) {
					SetModifierLabel(((ClassType) type).Modifier);
					cboModifier.Enabled = true;
				}
				else {
					cboModifier.Text = null;
					cboModifier.Enabled = false;
				}
			}
			else {
				txtName.Text = null;
				txtName.Enabled = false;
				cboAccess.Text = null;
				cboAccess.Enabled = false;
				cboModifier.Text = null;
				cboModifier.Enabled = false;
			}
		}

		private void SetAccessLabel(AccessModifier access)
		{
			AccessModifier[] accessOrder;

			if (project.Language == Language.CSharp)
				accessOrder = csharpAccessOrder;
			else
				accessOrder = javaAccessOrder;

			for (int i = 0; i < accessOrder.Length; i++) {
				if (accessOrder[i] == access) {
					cboAccess.SelectedIndex = i;
					return;
				}
			}

			cboAccess.SelectedIndex = 0;
		}

		private void SetModifierLabel(InheritanceModifier modifier)
		{
			InheritanceModifier[] modifierOrder;

			if (project.Language == Language.CSharp)
				modifierOrder = csharpModifierOrder;
			else
				modifierOrder = javaModifierOrder;


			for (int i = 0; i < modifierOrder.Length; i++) {
				if (modifierOrder[i] == modifier) {
					cboModifier.SelectedIndex = i;
					return;
				}
			}

			cboModifier.SelectedIndex = 0;
		}

		private void UpdateComboBoxes(Language language)
		{
			cboAccess.Items.Clear();
			cboModifier.Items.Clear();

			if (language == Language.CSharp) {
				cboAccess.Items.Add("Public");
				cboAccess.Items.Add("Protected Internal");
				cboAccess.Items.Add("Internal");
				cboAccess.Items.Add("Protected");
				cboAccess.Items.Add("Private");
				cboAccess.Items.Add("Default");

				cboModifier.Items.Add("None");
				cboModifier.Items.Add("Abstract");
				cboModifier.Items.Add("Sealed");
				cboModifier.Items.Add("Static");
			}
			else {
				cboAccess.Items.Add("Public");
				cboAccess.Items.Add("Protected");
				cboAccess.Items.Add("Private");
				cboAccess.Items.Add("Default");

				cboModifier.Items.Add("None");
				cboModifier.Items.Add("Abstract");
				cboModifier.Items.Add("Sealed");
				cboModifier.Items.Add("Static");
			}
		}

		private void project_StateChanged(object sender, EventArgs e)
		{
			UpdateHeader();
			UpdateStatusBar();
			UpdateComboBoxes(project.Language);

			if (!project.IsUntitled)
				Settings.AddRecentFile(project.ProjectFile);

			bool isNetLanguage = (project.Language == Language.CSharp);
			mnuNewDelegate.Visible = isNetLanguage;
			mnuNewDelegateContext.Visible = isNetLanguage;
			toolNewDelegate.Visible = isNetLanguage;
			mnuNewStruct.Visible = isNetLanguage;
			mnuNewStructContext.Visible = isNetLanguage;
			toolNewStruct.Visible = isNetLanguage;
		}

		private void diagram_SelectionChanged(object sender, EventArgs e)
		{
			UpdateStatusBar();
			UpdateModifiersToolBar();
			toolDelete.Enabled = (diagram.SelectedItemCount > 0);
		}

		private void Texts_LanguageChanged(object sender, EventArgs e)
		{
			UpdateTexts();
			UpdateStatusBar();
		}

		private void OpenRecentFile_Click(object sender, EventArgs e)
		{
			if (sender is ToolStripItem && ((ToolStripItem) sender).Tag is int) {
				int index = (int) ((ToolStripItem) sender).Tag;
				if (index >= 0 && index < Settings.RecentFileCount)
					LoadProject(Settings.GetRecentFile(index));
			}
		}

		private void optionsDialog_Apply(object sender, EventArgs e)
		{
			UpdateWindow();
		}

		private void printDocument_PrintPage(object sender, PrintPageEventArgs e)
		{
			diagram.Print(e);
		}

		#region File menu eventhandlers

		private void mnuNewCSharpDiagram_Click(object sender, EventArgs e)
		{
			project.NewProject(Language.CSharp);
		}

		private void mnuNewJavaDiagram_Click(object sender, EventArgs e)
		{
			project.NewProject(Language.Java);
		}

		private void mnuOpenFile_Click(object sender, EventArgs e)
		{
			project.Load();
		}

		private void mnuOpen_DropDownOpening(object sender, EventArgs e)
		{
			foreach (ToolStripItem item in mnuOpen.DropDownItems) {
				if (item.Tag is int) {
					int index = (int) item.Tag;

					if (index < Settings.RecentFileCount) {
						item.Text = Settings.GetRecentFile(index);
						item.Visible = true;
					}
					else {
						item.Visible = false;
					}
				}
			}

			sepOpenFile.Visible = (Settings.RecentFileCount > 0);
		}

		private void mnuSave_Click(object sender, EventArgs e)
		{
			project.Save();
		}

		private void mnuSaveAs_Click(object sender, EventArgs e)
		{
			project.SaveAs();
		}

		private void mnuPrintSetup_Click(object sender, EventArgs e)
		{
			Margins originalMargins = pageSetupDialog.PageSettings.Margins;

			if (System.Globalization.RegionInfo.CurrentRegion.IsMetric) {
				// This is necessary because of the bug in the PageSetupDialog control.
				// More information: http://support.microsoft.com/?id=814355
				pageSetupDialog.PageSettings.Margins = PrinterUnitConvert.Convert(
					pageSetupDialog.PageSettings.Margins,
					PrinterUnit.Display, PrinterUnit.TenthsOfAMillimeter);
			}

			if (pageSetupDialog.ShowDialog() != DialogResult.OK)
				pageSetupDialog.PageSettings.Margins = originalMargins; 
		}

		private void mnuPrintPreview_Click(object sender, EventArgs e)
		{
			printPreviewDialog.ShowDialog();
		}

		private void mnuPrint_Click(object sender, EventArgs e)
		{
			if (printDialog.ShowDialog() == DialogResult.OK)
				printDocument.Print();
		}

		private void mnuExit_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		#endregion

		#region Diagram menu eventhandlers

		private void mnuDiagram_DropDownOpening(object sender, EventArgs e)
		{
			mnuDelete.Enabled = (diagram.SelectedItemCount > 0);
			mnuSaveAsImage.Enabled = (diagram.ItemCount > 0);
		}

		private void mnuNewClass_Click(object sender, EventArgs e)
		{
			project.AddClass("NewClass");
		}

		private void mnuNewStruct_Click(object sender, EventArgs e)
		{
			project.AddStruct("NewStruct");
		}

		private void mnuNewInterface_Click(object sender, EventArgs e)
		{
			project.AddInterface("NewInterface");
		}

		private void mnuNewEnum_Click(object sender, EventArgs e)
		{
			project.AddEnum("NewEnum");
		}

		private void mnuNewDelegate_Click(object sender, EventArgs e)
		{
			if (project.Language == Language.CSharp)
				project.AddDelegate("NewDelegate");
		}

		private void mnuNewComment_Click(object sender, EventArgs e)
		{
			project.AddComment();
		}

		private void mnuNewAssociation_Click(object sender, EventArgs e)
		{
			diagram.StartConnection(RelationType.Association);
		}

		private void mnuNewComposition_Click(object sender, EventArgs e)
		{
			diagram.StartConnection(RelationType.Composition);
		}

		private void mnuNewAggregation_Click(object sender, EventArgs e)
		{
			diagram.StartConnection(RelationType.Aggregation);
		}

		private void mnuNewGeneralization_Click(object sender, EventArgs e)
		{
			diagram.StartConnection(RelationType.Generalization);
		}

		private void mnuNewRealization_Click(object sender, EventArgs e)
		{
			diagram.StartConnection(RelationType.Realization);
		}

		private void mnuNewDependency_Click(object sender, EventArgs e)
		{
			diagram.StartConnection(RelationType.Dependency);
		}

		private void mnuNewNesting_Click(object sender, EventArgs e)
		{
			diagram.StartConnection(RelationType.Nesting);
		}

		private void mnuNewCommentRelation_Click(object sender, EventArgs e)
		{
			diagram.StartConnection(RelationType.CommentRelation);
		}

		private void mnuMembersFormat_DropDownOpening(object sender, EventArgs e)
		{
			mnuShowType.Checked = Settings.ShowType;
			mnuShowParameters.Checked = Settings.ShowParameters;
			mnuShowParameterNames.Checked = Settings.ShowParameterNames;
			mnuShowInitialValue.Checked = Settings.ShowInitialValue;
		}

		private void mnuShowType_CheckedChanged(object sender, EventArgs e)
		{
			Settings.ShowType = ((ToolStripMenuItem) sender).Checked;
			diagram.Refresh();
		}

		private void mnuShowParameters_CheckedChanged(object sender, EventArgs e)
		{
			Settings.ShowParameters = ((ToolStripMenuItem) sender).Checked;
			diagram.Refresh();
		}

		private void mnuShowParameterNames_CheckedChanged(object sender, EventArgs e)
		{
			Settings.ShowParameterNames = ((ToolStripMenuItem) sender).Checked;
			diagram.Refresh();
		}

		private void mnuShowInitialValue_CheckedChanged(object sender, EventArgs e)
		{
			Settings.ShowInitialValue = ((ToolStripMenuItem) sender).Checked;
			diagram.Refresh();
		}

		private void mnuDelete_Click(object sender, EventArgs e)
		{
			diagram.RemoveSelectedItems();
		}

		private void mnuSaveAsImage_Click(object sender, EventArgs e)
		{
			SaveAsImage();
		}

		private void mnuAutoZoom_Click(object sender, EventArgs e)
		{
			MessageBox.Show(Texts.GetText("not_implemented"), "NClass",
				MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void mnuOptions_Click(object sender, EventArgs e)
		{
			if (optionsDialog.ShowDialog() == DialogResult.OK)
				UpdateWindow();
		}

		#endregion

		#region Help menu eventhandlers

		private void mnuContents_Click(object sender, EventArgs e)
		{
			MessageBox.Show(Texts.GetText("not_implemented"), "NClass",
				MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void mnuAbout_Click(object sender, EventArgs e)
		{
			using (AboutDialog dialog = new AboutDialog())
				dialog.ShowDialog();
		}
		
		#endregion

		#region Standard toolbar eventhandlers

		private void toolNew_ButtonClick(object sender, EventArgs e)
		{
			project.NewProject();
		}

		private void toolOpen_DropDownOpening(object sender, EventArgs e)
		{
			foreach (ToolStripItem item in toolOpen.DropDownItems) {
				if (item.Tag is int) {
					int index = (int) item.Tag;

					if (index < Settings.RecentFileCount) {
						item.Text = Settings.GetRecentFile(index);
						item.Visible = true;
					}
					else {
						item.Visible = false;
					}
				}
			}
		}

		private void toolZoomIn_Click(object sender, EventArgs e)
		{
			MessageBox.Show(Texts.GetText("not_implemented"), "NClass",
				MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void toolZoomOut_Click(object sender, EventArgs e)
		{
			MessageBox.Show(Texts.GetText("not_implemented"), "NClass",
				MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void cboZoom_KeyDown(object sender, KeyEventArgs e)
		{
			MessageBox.Show(Texts.GetText("not_implemented"), "NClass",
				MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void cboZoom_SelectedIndexChanged(object sender, EventArgs e)
		{
			MessageBox.Show(Texts.GetText("not_implemented"), "NClass",
				MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void cboZoom_Validated(object sender, EventArgs e)
		{
			MessageBox.Show(Texts.GetText("not_implemented"), "NClass",
				MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void toolAutoZoom_Click(object sender, EventArgs e)
		{
			MessageBox.Show(Texts.GetText("not_implemented"), "NClass",
				MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		#endregion

		#region Details toolbar eventhandlers

		private void txtName_TextChanged(object sender, EventArgs e)
		{
			if (diagram.SelectedItemCount == 1 && diagram.FirstSelectedItem is TypeShape) {
				TypeShape shape = (TypeShape) diagram.FirstSelectedItem;
				TypeBase type = (TypeBase) shape.Entity;

				if (type.Name != txtName.Text) {
					try {
						type.Name = txtName.Text;
						project.IsDirty = true;
						UpdateStatusBar();
						shape.Refresh();
					}
					catch (BadSyntaxException) {
						// Ignores the incorrect name
					}
				}
			}
		}

		private void cboAccess_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (diagram.SelectedItemCount == 1 && diagram.FirstSelectedItem is TypeShape) {
				TypeShape shape = (TypeShape) diagram.FirstSelectedItem;
				TypeBase type = (TypeBase) shape.Entity;

				int index = cboAccess.SelectedIndex;
				if (type.Language == Language.CSharp)
					type.AccessModifier = csharpAccessOrder[index];
				else
					type.AccessModifier = javaAccessOrder[index];

				shape.Refresh();
			}
		}

		private void cboModifier_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (diagram.SelectedItemCount == 1 && diagram.FirstSelectedItem is ClassShape) {
				ClassShape shape = (ClassShape) diagram.FirstSelectedItem;
				ClassType type = (ClassType) shape.Entity;

				int index = cboModifier.SelectedIndex;
				if (type.Language == Language.CSharp)
					type.Modifier = csharpModifierOrder[index];
				else
					type.Modifier = javaModifierOrder[index];

				shape.Refresh();
			}
		}

		#endregion

		#region Diagram context menu eventhandlers

		private void diagramContextMenuStrip_Opening(object sender, CancelEventArgs e)
		{
			mnuSelectAllContext.Enabled = (diagram.ItemCount > 0);
			mnuSaveAsImageContext.Enabled = (diagram.ItemCount > 0);
		}

		private void mnuSelectAllContext_Click(object sender, EventArgs e)
		{
			diagram.SelectAll();
			Texts.Language = DisplayLanguage.English;
		}

		private void mnuMembersFormatContext_DropDownOpening(object sender, EventArgs e)
		{
			mnuShowTypeContext.Checked = Settings.ShowType;
			mnuShowParametersContext.Checked = Settings.ShowParameters;
			mnuShowParameterNamesContext.Checked = Settings.ShowParameterNames;
			mnuShowInitialValueContext.Checked = Settings.ShowInitialValue;
		}

		#endregion
	}
}