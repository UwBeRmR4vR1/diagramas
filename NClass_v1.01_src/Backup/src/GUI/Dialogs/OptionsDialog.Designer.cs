﻿namespace NClass.GUI
{
	sealed partial class OptionsDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.grpDiagram = new System.Windows.Forms.GroupBox();
			this.chkPrivate = new System.Windows.Forms.CheckBox();
			this.chkProtected = new System.Windows.Forms.CheckBox();
			this.chkInternal = new System.Windows.Forms.CheckBox();
			this.chkProtectedInternal = new System.Windows.Forms.CheckBox();
			this.chkPublic = new System.Windows.Forms.CheckBox();
			this.lblVisibleMembers = new System.Windows.Forms.Label();
			this.chkUseSnapLines = new System.Windows.Forms.CheckBox();
			this.lblX = new System.Windows.Forms.Label();
			this.numWorkspaceHeight = new System.Windows.Forms.NumericUpDown();
			this.numWorkspaceWidth = new System.Windows.Forms.NumericUpDown();
			this.lblMinWorkspace = new System.Windows.Forms.Label();
			this.tabOptions = new System.Windows.Forms.TabControl();
			this.tabGeneral = new System.Windows.Forms.TabPage();
			this.grpGeneral = new System.Windows.Forms.GroupBox();
			this.lblLanguage = new System.Windows.Forms.Label();
			this.chkShowFullPath = new System.Windows.Forms.CheckBox();
			this.chkLoadLastProject = new System.Windows.Forms.CheckBox();
			this.btnClearRecents = new System.Windows.Forms.Button();
			this.cboLanguage = new System.Windows.Forms.ComboBox();
			this.tabStyle = new System.Windows.Forms.TabPage();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnLoad = new System.Windows.Forms.Button();
			this.stylePropertyGrid = new System.Windows.Forms.PropertyGrid();
			this.btnApply = new System.Windows.Forms.Button();
			this.grpDiagram.SuspendLayout();
			((System.ComponentModel.ISupportInitialize) (this.numWorkspaceHeight)).BeginInit();
			((System.ComponentModel.ISupportInitialize) (this.numWorkspaceWidth)).BeginInit();
			this.tabOptions.SuspendLayout();
			this.tabGeneral.SuspendLayout();
			this.grpGeneral.SuspendLayout();
			this.tabStyle.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(179, 408);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 2;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			// 
			// btnOK
			// 
			this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.Location = new System.Drawing.Point(98, 408);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(75, 23);
			this.btnOK.TabIndex = 1;
			this.btnOK.Text = "OK";
			this.btnOK.UseVisualStyleBackColor = true;
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// grpDiagram
			// 
			this.grpDiagram.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.grpDiagram.Controls.Add(this.chkPrivate);
			this.grpDiagram.Controls.Add(this.chkProtected);
			this.grpDiagram.Controls.Add(this.chkInternal);
			this.grpDiagram.Controls.Add(this.chkProtectedInternal);
			this.grpDiagram.Controls.Add(this.chkPublic);
			this.grpDiagram.Controls.Add(this.lblVisibleMembers);
			this.grpDiagram.Controls.Add(this.chkUseSnapLines);
			this.grpDiagram.Controls.Add(this.lblX);
			this.grpDiagram.Controls.Add(this.numWorkspaceHeight);
			this.grpDiagram.Controls.Add(this.numWorkspaceWidth);
			this.grpDiagram.Controls.Add(this.lblMinWorkspace);
			this.grpDiagram.Location = new System.Drawing.Point(6, 139);
			this.grpDiagram.Name = "grpDiagram";
			this.grpDiagram.Size = new System.Drawing.Size(304, 219);
			this.grpDiagram.TabIndex = 3;
			this.grpDiagram.TabStop = false;
			this.grpDiagram.Text = "Diagram";
			// 
			// chkPrivate
			// 
			this.chkPrivate.AutoSize = true;
			this.chkPrivate.Location = new System.Drawing.Point(9, 190);
			this.chkPrivate.Name = "chkPrivate";
			this.chkPrivate.Size = new System.Drawing.Size(59, 17);
			this.chkPrivate.TabIndex = 11;
			this.chkPrivate.Text = "Private";
			this.chkPrivate.UseVisualStyleBackColor = true;
			this.chkPrivate.CheckedChanged += new System.EventHandler(this.SettingsStateChanged);
			// 
			// chkProtected
			// 
			this.chkProtected.AutoSize = true;
			this.chkProtected.Location = new System.Drawing.Point(9, 167);
			this.chkProtected.Name = "chkProtected";
			this.chkProtected.Size = new System.Drawing.Size(72, 17);
			this.chkProtected.TabIndex = 10;
			this.chkProtected.Text = "Protected";
			this.chkProtected.UseVisualStyleBackColor = true;
			this.chkProtected.CheckedChanged += new System.EventHandler(this.SettingsStateChanged);
			// 
			// chkInternal
			// 
			this.chkInternal.AutoSize = true;
			this.chkInternal.Location = new System.Drawing.Point(9, 144);
			this.chkInternal.Name = "chkInternal";
			this.chkInternal.Size = new System.Drawing.Size(113, 17);
			this.chkInternal.TabIndex = 9;
			this.chkInternal.Text = "Internal (Package)";
			this.chkInternal.UseVisualStyleBackColor = true;
			this.chkInternal.CheckedChanged += new System.EventHandler(this.SettingsStateChanged);
			// 
			// chkProtectedInternal
			// 
			this.chkProtectedInternal.AutoSize = true;
			this.chkProtectedInternal.Location = new System.Drawing.Point(9, 121);
			this.chkProtectedInternal.Name = "chkProtectedInternal";
			this.chkProtectedInternal.Size = new System.Drawing.Size(110, 17);
			this.chkProtectedInternal.TabIndex = 8;
			this.chkProtectedInternal.Text = "Protected Internal";
			this.chkProtectedInternal.UseVisualStyleBackColor = true;
			this.chkProtectedInternal.CheckedChanged += new System.EventHandler(this.SettingsStateChanged);
			// 
			// chkPublic
			// 
			this.chkPublic.AutoSize = true;
			this.chkPublic.Location = new System.Drawing.Point(9, 98);
			this.chkPublic.Name = "chkPublic";
			this.chkPublic.Size = new System.Drawing.Size(55, 17);
			this.chkPublic.TabIndex = 7;
			this.chkPublic.Text = "Public";
			this.chkPublic.UseVisualStyleBackColor = true;
			this.chkPublic.CheckedChanged += new System.EventHandler(this.SettingsStateChanged);
			// 
			// lblVisibleMembers
			// 
			this.lblVisibleMembers.AutoSize = true;
			this.lblVisibleMembers.Location = new System.Drawing.Point(6, 80);
			this.lblVisibleMembers.Name = "lblVisibleMembers";
			this.lblVisibleMembers.Size = new System.Drawing.Size(85, 13);
			this.lblVisibleMembers.TabIndex = 6;
			this.lblVisibleMembers.Text = "Visible members:";
			// 
			// chkUseSnapLines
			// 
			this.chkUseSnapLines.AutoSize = true;
			this.chkUseSnapLines.Location = new System.Drawing.Point(9, 20);
			this.chkUseSnapLines.Name = "chkUseSnapLines";
			this.chkUseSnapLines.Size = new System.Drawing.Size(136, 17);
			this.chkUseSnapLines.TabIndex = 0;
			this.chkUseSnapLines.Text = "Use precision snapping";
			this.chkUseSnapLines.UseVisualStyleBackColor = true;
			this.chkUseSnapLines.CheckedChanged += new System.EventHandler(this.SettingsStateChanged);
			// 
			// lblX
			// 
			this.lblX.AutoSize = true;
			this.lblX.Location = new System.Drawing.Point(183, 45);
			this.lblX.Name = "lblX";
			this.lblX.Size = new System.Drawing.Size(13, 13);
			this.lblX.TabIndex = 4;
			this.lblX.Text = "×";
			// 
			// numWorkspaceHeight
			// 
			this.numWorkspaceHeight.Location = new System.Drawing.Point(199, 43);
			this.numWorkspaceHeight.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.numWorkspaceHeight.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
			this.numWorkspaceHeight.Name = "numWorkspaceHeight";
			this.numWorkspaceHeight.Size = new System.Drawing.Size(71, 20);
			this.numWorkspaceHeight.TabIndex = 3;
			this.numWorkspaceHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.numWorkspaceHeight.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
			this.numWorkspaceHeight.ValueChanged += new System.EventHandler(this.SettingsStateChanged);
			// 
			// numWorkspaceWidth
			// 
			this.numWorkspaceWidth.Location = new System.Drawing.Point(109, 43);
			this.numWorkspaceWidth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.numWorkspaceWidth.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
			this.numWorkspaceWidth.Name = "numWorkspaceWidth";
			this.numWorkspaceWidth.Size = new System.Drawing.Size(71, 20);
			this.numWorkspaceWidth.TabIndex = 2;
			this.numWorkspaceWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.numWorkspaceWidth.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
			this.numWorkspaceWidth.ValueChanged += new System.EventHandler(this.SettingsStateChanged);
			// 
			// lblMinWorkspace
			// 
			this.lblMinWorkspace.AutoSize = true;
			this.lblMinWorkspace.Location = new System.Drawing.Point(6, 45);
			this.lblMinWorkspace.Name = "lblMinWorkspace";
			this.lblMinWorkspace.Size = new System.Drawing.Size(100, 13);
			this.lblMinWorkspace.TabIndex = 1;
			this.lblMinWorkspace.Text = "Minimal workspace:";
			// 
			// tabOptions
			// 
			this.tabOptions.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.tabOptions.Controls.Add(this.tabGeneral);
			this.tabOptions.Controls.Add(this.tabStyle);
			this.tabOptions.Location = new System.Drawing.Point(12, 12);
			this.tabOptions.Multiline = true;
			this.tabOptions.Name = "tabOptions";
			this.tabOptions.SelectedIndex = 0;
			this.tabOptions.Size = new System.Drawing.Size(323, 390);
			this.tabOptions.TabIndex = 5;
			// 
			// tabGeneral
			// 
			this.tabGeneral.Controls.Add(this.grpGeneral);
			this.tabGeneral.Controls.Add(this.grpDiagram);
			this.tabGeneral.Location = new System.Drawing.Point(4, 22);
			this.tabGeneral.Name = "tabGeneral";
			this.tabGeneral.Padding = new System.Windows.Forms.Padding(3);
			this.tabGeneral.Size = new System.Drawing.Size(315, 364);
			this.tabGeneral.TabIndex = 0;
			this.tabGeneral.Text = "General";
			this.tabGeneral.UseVisualStyleBackColor = true;
			// 
			// grpGeneral
			// 
			this.grpGeneral.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.grpGeneral.Controls.Add(this.lblLanguage);
			this.grpGeneral.Controls.Add(this.chkShowFullPath);
			this.grpGeneral.Controls.Add(this.chkLoadLastProject);
			this.grpGeneral.Controls.Add(this.btnClearRecents);
			this.grpGeneral.Controls.Add(this.cboLanguage);
			this.grpGeneral.Location = new System.Drawing.Point(6, 9);
			this.grpGeneral.Name = "grpGeneral";
			this.grpGeneral.Size = new System.Drawing.Size(304, 124);
			this.grpGeneral.TabIndex = 1;
			this.grpGeneral.TabStop = false;
			this.grpGeneral.Text = "General";
			// 
			// lblLanguage
			// 
			this.lblLanguage.AutoSize = true;
			this.lblLanguage.Location = new System.Drawing.Point(6, 20);
			this.lblLanguage.Name = "lblLanguage";
			this.lblLanguage.Size = new System.Drawing.Size(58, 13);
			this.lblLanguage.TabIndex = 0;
			this.lblLanguage.Text = "Language:";
			// 
			// chkShowFullPath
			// 
			this.chkShowFullPath.AutoSize = true;
			this.chkShowFullPath.Location = new System.Drawing.Point(9, 67);
			this.chkShowFullPath.Name = "chkShowFullPath";
			this.chkShowFullPath.Size = new System.Drawing.Size(174, 17);
			this.chkShowFullPath.TabIndex = 3;
			this.chkShowFullPath.Text = "Show full file path in the header";
			this.chkShowFullPath.UseVisualStyleBackColor = true;
			this.chkShowFullPath.CheckedChanged += new System.EventHandler(this.SettingsStateChanged);
			// 
			// chkLoadLastProject
			// 
			this.chkLoadLastProject.AutoSize = true;
			this.chkLoadLastProject.Location = new System.Drawing.Point(9, 44);
			this.chkLoadLastProject.Name = "chkLoadLastProject";
			this.chkLoadLastProject.Size = new System.Drawing.Size(143, 17);
			this.chkLoadLastProject.TabIndex = 2;
			this.chkLoadLastProject.Text = "Load last opened project";
			this.chkLoadLastProject.UseVisualStyleBackColor = true;
			this.chkLoadLastProject.CheckedChanged += new System.EventHandler(this.SettingsStateChanged);
			// 
			// btnClearRecents
			// 
			this.btnClearRecents.AutoSize = true;
			this.btnClearRecents.Location = new System.Drawing.Point(9, 90);
			this.btnClearRecents.Name = "btnClearRecents";
			this.btnClearRecents.Size = new System.Drawing.Size(110, 23);
			this.btnClearRecents.TabIndex = 4;
			this.btnClearRecents.Text = "Clear recent files list";
			this.btnClearRecents.UseVisualStyleBackColor = true;
			// 
			// cboLanguage
			// 
			this.cboLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboLanguage.FormattingEnabled = true;
			this.cboLanguage.Location = new System.Drawing.Point(67, 17);
			this.cboLanguage.Name = "cboLanguage";
			this.cboLanguage.Size = new System.Drawing.Size(121, 21);
			this.cboLanguage.TabIndex = 1;
			this.cboLanguage.SelectedIndexChanged += new System.EventHandler(this.SettingsStateChanged);
			// 
			// tabStyle
			// 
			this.tabStyle.Controls.Add(this.btnSave);
			this.tabStyle.Controls.Add(this.btnLoad);
			this.tabStyle.Controls.Add(this.stylePropertyGrid);
			this.tabStyle.Location = new System.Drawing.Point(4, 22);
			this.tabStyle.Name = "tabStyle";
			this.tabStyle.Padding = new System.Windows.Forms.Padding(3);
			this.tabStyle.Size = new System.Drawing.Size(315, 364);
			this.tabStyle.TabIndex = 1;
			this.tabStyle.Text = "Style";
			this.tabStyle.UseVisualStyleBackColor = true;
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave.Location = new System.Drawing.Point(234, 335);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 23);
			this.btnSave.TabIndex = 4;
			this.btnSave.Text = "Save";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnLoad
			// 
			this.btnLoad.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnLoad.Location = new System.Drawing.Point(153, 335);
			this.btnLoad.Name = "btnLoad";
			this.btnLoad.Size = new System.Drawing.Size(75, 23);
			this.btnLoad.TabIndex = 3;
			this.btnLoad.Text = "Load";
			this.btnLoad.UseVisualStyleBackColor = true;
			this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
			// 
			// stylePropertyGrid
			// 
			this.stylePropertyGrid.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.stylePropertyGrid.Location = new System.Drawing.Point(6, 6);
			this.stylePropertyGrid.Name = "stylePropertyGrid";
			this.stylePropertyGrid.Size = new System.Drawing.Size(303, 323);
			this.stylePropertyGrid.TabIndex = 0;
			this.stylePropertyGrid.ToolbarVisible = false;
			this.stylePropertyGrid.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.stylePropertyGrid_PropertyValueChanged);
			// 
			// btnApply
			// 
			this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnApply.Enabled = false;
			this.btnApply.Location = new System.Drawing.Point(260, 408);
			this.btnApply.Name = "btnApply";
			this.btnApply.Size = new System.Drawing.Size(75, 23);
			this.btnApply.TabIndex = 6;
			this.btnApply.Text = "Apply";
			this.btnApply.UseVisualStyleBackColor = true;
			this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
			// 
			// OptionsDialog
			// 
			this.AcceptButton = this.btnOK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(347, 443);
			this.Controls.Add(this.btnApply);
			this.Controls.Add(this.tabOptions);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "OptionsDialog";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Options";
			this.grpDiagram.ResumeLayout(false);
			this.grpDiagram.PerformLayout();
			((System.ComponentModel.ISupportInitialize) (this.numWorkspaceHeight)).EndInit();
			((System.ComponentModel.ISupportInitialize) (this.numWorkspaceWidth)).EndInit();
			this.tabOptions.ResumeLayout(false);
			this.tabGeneral.ResumeLayout(false);
			this.grpGeneral.ResumeLayout(false);
			this.grpGeneral.PerformLayout();
			this.tabStyle.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.GroupBox grpDiagram;
		private System.Windows.Forms.TabControl tabOptions;
		private System.Windows.Forms.TabPage tabGeneral;
		private System.Windows.Forms.GroupBox grpGeneral;
		private System.Windows.Forms.Label lblLanguage;
		private System.Windows.Forms.CheckBox chkShowFullPath;
		private System.Windows.Forms.CheckBox chkLoadLastProject;
		private System.Windows.Forms.Button btnClearRecents;
		private System.Windows.Forms.ComboBox cboLanguage;
		private System.Windows.Forms.TabPage tabStyle;
		private System.Windows.Forms.PropertyGrid stylePropertyGrid;
		private System.Windows.Forms.Button btnApply;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnLoad;
		private System.Windows.Forms.NumericUpDown numWorkspaceWidth;
		private System.Windows.Forms.Label lblMinWorkspace;
		private System.Windows.Forms.CheckBox chkUseSnapLines;
		private System.Windows.Forms.Label lblX;
		private System.Windows.Forms.NumericUpDown numWorkspaceHeight;
		private System.Windows.Forms.Label lblVisibleMembers;
		private System.Windows.Forms.CheckBox chkPrivate;
		private System.Windows.Forms.CheckBox chkProtected;
		private System.Windows.Forms.CheckBox chkInternal;
		private System.Windows.Forms.CheckBox chkProtectedInternal;
		private System.Windows.Forms.CheckBox chkPublic;



	}
}