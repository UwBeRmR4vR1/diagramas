﻿// NClass - UML class diagram editor
// Copyright (C) 2006 Balázs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.Windows.Forms;
using NClass.Translations;
using System.Reflection;

namespace NClass.GUI
{
	public partial class AboutDialog : Form
	{
		const string MailAddress = "btihanyi@users.sourceforge.net";
		const string WebAddress  = "http://sourceforge.net/projects/nclass/";

		static readonly Assembly assembly = Assembly.GetExecutingAssembly();

		public AboutDialog()
		{
			InitializeComponent();
		}

		private void UpdateTexts()
		{
			Version version = assembly.GetName().Version;

			this.Text = Texts.GetText("about_nclass");
			lblTitle.Text = string.Format("NClass {0}.{1}", version.Major, version.Minor);
			lblCopyright.Text = "Copyright (C) 2006 " + Texts.GetText("author");
			lblStatus.Text = Texts.GetText("version", Texts.GetText("alpha"));
			lnkEmail.Text = Texts.GetText("send_email");
			lnkHomepage.Text = Texts.GetText("visit_homepage");
			btnClose.Text = Texts.GetText("button_close");

			lnkHomepage.Links.Clear();
			lnkEmail.Links.Clear();
			lnkHomepage.Links.Add(0, lnkHomepage.Text.Length, WebAddress);
			lnkEmail.Links.Add(0, lnkEmail.Text.Length,
				"mailto:" + MailAddress + "?subject=NClass");
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			UpdateTexts();
		}

		private void lnkEmail_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			string target = e.Link.LinkData as string;

			if (target != null) {
				try {
					System.Diagnostics.Process.Start(target);
				}
				catch (Exception ex) {
					MessageBox.Show(
						string.Format("{0}\n{1}: {2}", Texts.GetText("command_failed"),
							Texts.GetText("errors_reason"), ex.Message),
						Texts.GetText("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
		}

		private void lnkHomepage_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			string target = e.Link.LinkData as string;

			if (target != null) {
				try {
					System.Diagnostics.Process.Start(target);
				}
				catch (Exception ex) {
					MessageBox.Show(
						string.Format("{0}\n{1}: {2}", Texts.GetText("command_failed"),
							Texts.GetText("errors_reason"), ex.Message),
						Texts.GetText("error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}