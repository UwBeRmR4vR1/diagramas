﻿// NClass - UML class diagram editor
// Copyright (C) 2006 Balázs Tihanyi
// 
// This program is free software; you can redistribute it and/or modify it under 
// the terms of the GNU General Public License as published by the Free Software 
// Foundation; either version 3 of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

using System;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using NClass.GUI.Diagram;
using NClass.Translations;

namespace NClass.GUI
{
	public sealed partial class OptionsDialog : Form
	{
		static string stylesPath;
		Style tempStyle = null;

		public event EventHandler Apply;

		static OptionsDialog()
		{
			try {
				stylesPath = Path.Combine(
					Directory.GetParent(Application.StartupPath).FullName, "styles");
			}
			catch {
				stylesPath = Application.StartupPath;
			}
		}

		public OptionsDialog()
		{
			InitializeComponent();
		}

		private Style TempStyle
		{
			get
			{
				return tempStyle;
			}
			set
			{
				if (value != null) {
					if (value == Style.CurrentStyle)
						tempStyle = (Style) value.Clone();
					else
						tempStyle = value;
					stylePropertyGrid.SelectedObject = tempStyle;
				}
			}
		}

		private void UpdateTexts()
		{
			this.Text = Texts.GetText("options");
			tabGeneral.Text = Texts.GetText("general");
			tabStyle.Text = Texts.GetText("style");
			grpGeneral.Text = Texts.GetText("general");
			grpDiagram.Text = Texts.GetText("diagram");
			lblLanguage.Text = Texts.GetText("language") + ":";
			chkLoadLastProject.Text = Texts.GetText("load_last_project");
			chkShowFullPath.Text = Texts.GetText("show_full_path");
			btnClearRecents.Text = Texts.GetText("clear_recent_list");
			chkUseSnapLines.Text = Texts.GetText("use_precision_snapping");
			lblMinWorkspace.Text = Texts.GetText("minimal_workspace");
			lblVisibleMembers.Text = Texts.GetText("visible_members:");

			cboLanguage.Left = lblLanguage.Left + lblLanguage.Width + 3;
			numWorkspaceWidth.Left = lblMinWorkspace.Left + lblMinWorkspace.Width + 3;
			lblX.Left = numWorkspaceWidth.Left + numWorkspaceWidth.Width + 3;
			numWorkspaceHeight.Left = lblX.Left + lblX.Width + 3;

			btnLoad.Text = Texts.GetText("button_load");
			btnSave.Text = Texts.GetText("button_save");
			btnOK.Text = Texts.GetText("button_ok");
			btnCancel.Text = Texts.GetText("button_cancel");
			btnApply.Text = Texts.GetText("button_apply");
		}

		private void FillLanguages()
		{
			cboLanguage.Items.Clear();
			foreach (string name in Enum.GetNames(typeof(DisplayLanguage)))
				cboLanguage.Items.Add(name);
		}

		private void LoadSettings()
		{
			// General settings			
			cboLanguage.SelectedIndex = (int) Texts.Language;
			chkLoadLastProject.Checked = Settings.LoadLastProject;
			chkShowFullPath.Checked = Settings.ShowFullFilePath;

			// Diagram settings
			chkUseSnapLines.Checked = Settings.UsePrecisionSnapping;
			numWorkspaceWidth.Value = Settings.WorkspaceWidth;
			numWorkspaceHeight.Value = Settings.WorkspaceHeight;
			chkPublic.Checked = Settings.IsPublicVisible;
			chkProtectedInternal.Checked = Settings.IsProtintVisible;
			chkInternal.Checked = Settings.IsInternalVisible;
			chkProtected.Checked = Settings.IsProtectedVisible;
			chkPrivate.Checked = Settings.IsPrivateVisible;

			// Style settings
			TempStyle = Style.CurrentStyle;
		}

		private void SaveSettings()
		{
			// General settings			
			Texts.Language = (DisplayLanguage) cboLanguage.SelectedIndex;
			Settings.LoadLastProject = chkLoadLastProject.Checked;
			Settings.ShowFullFilePath = chkShowFullPath.Checked;

			// Diagram settings
			Settings.UsePrecisionSnapping = chkUseSnapLines.Checked;
			Settings.WorkspaceWidth = (int) numWorkspaceWidth.Value;
			Settings.WorkspaceHeight = (int) numWorkspaceHeight.Value;
			Settings.IsPublicVisible = chkPublic.Checked;
			Settings.IsProtintVisible = chkProtectedInternal.Checked;
			Settings.IsInternalVisible = chkInternal.Checked;
			Settings.IsProtectedVisible = chkProtected.Checked;
			Settings.IsPrivateVisible = chkPrivate.Checked;

			// Style settings
			Style.CurrentStyle = TempStyle;
			TempStyle = Style.CurrentStyle;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			UpdateTexts();
			FillLanguages();
			LoadSettings();
			btnApply.Enabled = false;
		}

		protected override void OnClosed(EventArgs e)
		{
			if (TempStyle != null)
				TempStyle.Dispose();

			base.OnClosed(e);
		}

		private void SettingsStateChanged(object sender, EventArgs e)
		{
			btnApply.Enabled = true;
		}

		private void stylePropertyGrid_PropertyValueChanged(object s,
			PropertyValueChangedEventArgs e)
		{
			SettingsStateChanged(s, e);
		}

		private void btnClearRecents_Click(object sender, EventArgs e)
		{
			Settings.ClearRecentsList();
		}

		private void btnLoad_Click(object sender, EventArgs e)
		{
			using (OpenFileDialog dialog = new OpenFileDialog()) {
				dialog.Filter = Texts.GetText("diagram_style") + " (*.dst)|*.dst";
				dialog.InitialDirectory = stylesPath;

				if (dialog.ShowDialog() == DialogResult.OK) {
					Style style = Style.Load(dialog.FileName);

					if (style == null) {
						MessageBox.Show(
							Texts.GetText("error_could_not_load_file"), Texts.GetText("load"),
							MessageBoxButtons.OK,MessageBoxIcon.Error);
					}
					else {
						TempStyle.Dispose();
						TempStyle = style;
						btnApply.Enabled = true;
					}
				}
			}
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			using (SaveFileDialog dialog = new SaveFileDialog()) {
				dialog.FileName = TempStyle.Name;
				dialog.InitialDirectory = stylesPath;
				dialog.Filter = Texts.GetText("diagram_style") + " (*.dst)|*.dst";

				if (dialog.ShowDialog() == DialogResult.OK) {
					if (!TempStyle.Save(dialog.FileName)) {
						MessageBox.Show(
							Texts.GetText("error_could_not_save_file"), Texts.GetText("save"),
							MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}
			}
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			SaveSettings();
		}

		private void btnApply_Click(object sender, EventArgs e)
		{
			SaveSettings();
			UpdateTexts();
			if (Apply != null)
				Apply(this, e);
			btnApply.Enabled = false;
		}
	}
}